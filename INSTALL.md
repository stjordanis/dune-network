NB. This INSTALL guide is to short for you ? Read the following
[documentation](https://dune.network/docs/dune-node-mainnet/introduction/howtoget.html#build-from-sources)

Repository
----------

If not already done, the first step to get the sources is to clone the Git repository:
```
git clone https://gitlab.com/dune-network/dune-network.git
```

Then, move to cloned repository with:
```
cd dune-network
```

Branches
--------

By default, you are on the `next` branch, that targets the Testnet
network.

You may want to switch to the `mainnet` branch to target the
Mainnet network:
```
git checkout mainnet
```

To update mainnet branch to pull latest modifications pushed on the remote Git repository, you can use:
```
git pull -f origin mainnet
```

Building
--------

The typical commands to build Dune Network binaries are:
```
make build-deps
eval $(opam env)
make
```

The first one creates a local OPAM switch in dune-network/_opam, installs an
OCaml version and all needed OPAM dependencies.  This step is quite long the
first time it's ran. But, it's rather faster next times: (a) either there are no
changes in the dependencies, and the command has nothing to do, (b) or some
external packages are updated or added, which triggers a partial
update. Sometimes, updates with this command may fail. In this case, try running
the command again or remove `_opam` directory and re-generate it.

The second command is important to use OCaml environment in `_opam`
directory. The third one allows to build Dune Network libraries and binaries.

On Mac OS X, you may need before to define:
```
export OCAMLPARAM=_,ccopt=-Wno-typedef-redefinition,ccopt=-Wno-pointer-sign
```
if you have errors in the Hacl directory.

Basic Usage
-----------

To run a node, you need to create an identity the first time:

```
dune-node identity generate
```

You can then run the node:
```
dune-node run
```

To run a baker:
```
dune-baker-004-Pt24m4xi run with local node ~/.dune-node <baker-alias>
```

`004-Pt24m4xi` is the current protocol, you may have to change it, or to run two
bakers during major protocol updates. For instance:
```
dune-baker-004-Pt24m4xi run with local node ~/.dune-node
dune-baker-005-XXX run with local node ~/.dune-node
```

You SHOULD NEVER run the same baker binary twice! If you do it, you'll expose
yourself to double-baking issues and lose your deposits.


To run an endorser:
```
dune-endorser-004-Pt24m4xi run <baker-alias>
```

To run an accuser:
```
dune-accuser-004-Pt24m4xi run
```
