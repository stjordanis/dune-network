#! /bin/sh

## `ocaml-version` should be in sync with `README.rst` and
## `lib.protocol-compiler/tezos-protocol-compiler.opam`

ocaml_version=4.07.1
opam_version=2

## Please update `.gitlab-ci.yml` accordingly
opam_repository_tag=3a9b10831c1656ce61c29aef0ff2864abad4f1cf
full_opam_repository_tag=4c2bd0f948ae6a0fc602f73631cc8a8ae6e83c1d
opam_repository_url=https://gitlab.com/dune-network/opam-repository.git
opam_repository=$opam_repository_url\#$opam_repository_tag
