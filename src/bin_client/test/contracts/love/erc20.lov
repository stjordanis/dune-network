#love

type account = {
  balance : int;
  allowances : (address, int) map;
}

type storage = {
  accounts : (address, account) map;
  version : int; (* version of token standard *)
  totalSupply : int;
  decimals : int;
  name : string;
  symbol : string;
  owner : address;
}

val get_account (a : address) (accounts : (address, account) map) : account =
  match Map.find [:address] [:account] a accounts with
  | None -> { balance = 0; allowances = Map.empty [:address] [:int] }
  | Some account -> account

val perform_transfer (from : address) (dest : address)
    (tokens : int) (storage : storage) : (operation list * storage) =
  let accounts = storage.accounts in
  let account_sender = get_account from accounts in
  let new_balance = account_sender.balance - tokens in
  let new_account_sender =
    if new_balance < [:int] 0 then
      failwith [:string * int]
        ("Not enough tokens for transfer", account_sender.balance) [:account]
    else
      { account_sender with balance = new_balance }
  in
  let accounts = Map.add [:address] [:account]
                   from new_account_sender accounts in
  let account_dest = get_account dest accounts in
  let new_account_dest =
    { account_dest with balance = account_dest.balance + tokens } in
  let accounts = Map.add [:address] [:account]
                   dest new_account_dest accounts in
  [] [:operation], { storage with accounts = accounts }

val%entry transfer storage duns ((dest, tokens) : (address * int)) =
  perform_transfer (Current.sender ()) dest tokens storage

val%entry approve storage duns ((spender, tokens) : (address * int)) =
  let account_sender = get_account (Current.sender ()) storage.accounts in
  let account_sender =
    { account_sender with allowances =
      if tokens = [:int] 0 then
        Map.remove [:address] [:int]
          spender account_sender.allowances
      else
        Map.add [:address] [:int]
          spender tokens account_sender.allowances
    } in
  let storage = { storage with accounts =
      Map.add [:address] [:account]
        (Current.sender ()) account_sender storage.accounts
    } in
  [] [:operation], storage

val%entry transferFrom storage duns
      ((from, dest, tokens) : (address * address * int)) =
  let account_from = get_account from storage.accounts in
  let new_allowances_from =
    match Map.find [:address] [:int]
            (Current.sender ()) account_from.allowances with
    | None ->
        failwith [:string * address]
          ("Not allowed to spend from", from)
          [:(address, int) map]
    | Some allowed ->
      let allowed = allowed - tokens in
      if allowed < [:int] 0 then
        failwith [:string * int]
          ("Not enough allowance for transfer", allowed)
          [:(address, int) map]
      else if allowed = [:int] 0 then
        Map.remove [:address] [:int]
          (Current.sender ()) account_from.allowances
      else
        Map.add [:address] [:int]
          (Current.sender ()) allowed account_from.allowances
  in
  let account_from = { account_from with allowances = new_allowances_from } in
  let storage = { storage with accounts =
      Map.add [:address] [:account] from account_from storage.accounts
    } in
  perform_transfer from dest tokens storage


(* ------------- Storage access from outside ------------- *)

contract type NatContract = sig
  val%entry main : int
end

contract type NatNatContract = sig
  val%entry main : int * int
end

val%entry balanceOf storage duns ((spender, forward) :
  (address * instance NatContract)) =
  let spender_balance =
    match Map.find [:address] [:account] spender storage.accounts with
    | None -> 0
    | Some account -> account.balance
  in
  let (contract F : NatContract) = forward in
  let op = Contract.call [:int] F.main 0dn spender_balance in
  [op] [:operation], storage

val%entry allowance storage duns
  ((from, spender, forward) : (address * address * instance NatNatContract)) =
  let spender_allowance =
    match Map.find [:address] [:account] from storage.accounts with
    | None -> 0, 0
    | Some account ->
      match Map.find [:address] [:int] spender account.allowances with
      | None -> 0, account.balance
      | Some allowed -> allowed, account.balance
  in
  let (contract F : NatNatContract) = forward in
  let op = Contract.call [:int * int] F.main 0dn spender_allowance in
  [op] [:operation], storage


(* -------------- Creating accounts ------------------------ *)

val%entry createAccount storage duns ((dest, tokens) : (address * int)) =
  if Current.sender () <> [:address] storage.owner then
    failwith [:string] "Only owner can create accounts" [:unit];
  perform_transfer storage.owner dest tokens storage

val%entry createAccounts storage duns (new_accounts : ((address * int) list)) =
  if Current.sender () <> [:address] storage.owner then
    failwith [:string] "Only owner can create accounts" [:unit];
  List.fold [:address * int] [:operation list * storage]
    (fun ((dest, tokens) : (address * int))
      ((_ops, storage) : (operation list * storage)) ->
        perform_transfer storage.owner dest tokens storage
    ) new_accounts ([] [:operation], storage)

