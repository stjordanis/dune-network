(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Dune_std.OP
open Ix_types

let bindir = Ix_common.bindir

let node_dir c =
  match Ix_common.node_dir c c.current_node with
  | None ->
      Printf.eprintf "Error: current node is not local\n%!" ;
      exit 2
  | Some node_dir ->
      node_dir

let node_tool_log c tool =
  let node_dir = node_dir c in
  (node_dir // tool) ^ ".log"

let command c args ~lazy_flag ~one_flag =
  let node_dir =
    match Ix_common.node_dir c c.current_node with
    | None ->
        Printf.eprintf "Error: current node is not local\n%!" ;
        exit 2
    | Some node_dir ->
        node_dir
  in
  let args =
    match args with
    | ["god"] ->
        []
    | _ :: _ ->
        args
    | [] ->
        let bakers =
          List.fold_left
            (fun bakers account ->
              if account.account_is_baker then account.account_name :: bakers
              else bakers)
            []
            c.current_switch_state.switch_accounts
        in
        if bakers = [] then (
          Printf.eprintf "Error: you must specify at least one baker\n%!" ;
          exit 2 )
        else bakers
  in
  if one_flag then
    let baker = match args with [] -> "god" | baker :: _ -> baker in
    Ix_common.exec c ~set_bindir ~cmd:"dune-client" ~args:["bake"; "for"; baker]
  else
    let baker_log = node_tool_log c "baker" in
    let endorser_log = node_tool_log c "endorser" in
    let baker_pid =
      Ix_common.daemon
        c
        ~set_bindir
        ~log:baker_log
        ~cmd:"dune-baker-all"
        ~args:
          ( ["run"; "with"; "local"; "node"; node_dir]
          @ (if lazy_flag then ["--lazy"] else [])
          @ args )
    in
    let endorser_pid =
      Ix_common.daemon
        c
        ~set_bindir
        ~log:endorser_log
        ~cmd:"dune-endorser-all"
        ~args:(["run"] @ args)
    in
    Printf.eprintf
      "Baker and Endorser started. Use these commands to watch the logs:\n%!" ;
    Printf.eprintf "   tail -f %s\n%!" baker_log ;
    Printf.eprintf "   tail -f %s\n%!" endorser_log ;
    Ix_common.waitpid baker_pid ;
    Ix_common.waitpid endorser_pid

let ezcmd c =
  let lazy_flag = ref false in
  let one_flag = ref false in
  let anon_args = ref [] in
  Ix_common.ezcmd
    "bake"
    ~args:
      Ezcmd.Modules.
        [ ( ["lazy"],
            Arg.Set lazy_flag,
            Ezcmd.info "Work lazily, adding a block only when needed" );
          (["one"], Arg.Set one_flag, Ezcmd.info "Bake only one block") ]
    ~all:(fun args -> anon_args := args)
    ~action:(fun () ->
      command c !anon_args ~lazy_flag:!lazy_flag ~one_flag:!one_flag)
    ~doc:"Start a baker and endorser on the current node"
