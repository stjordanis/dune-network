(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

type config_args = {
  mutable change : bool;
  mutable singleprocess : bool option;
  mutable context_storage : string option;
}

let change c = c.change <- true

let config_args () =
  let c = {change = false; singleprocess = None; context_storage = None} in
  let spec =
    Ezcmd.Modules.
      [ ( ["single-process"],
          Arg.Unit
            (fun () ->
              change c ;
              c.singleprocess <- Some true),
          Ezcmd.info "Set the node to run with --singleprocess" );
        ( ["external-validator"],
          Arg.Unit
            (fun () ->
              change c ;
              c.singleprocess <- Some false),
          Ezcmd.info "Set the node to run without --singleprocess" );
        ( ["ironmin"],
          Arg.Unit
            (fun () ->
              change c ;
              c.context_storage <- Some "ironmin"),
          Ezcmd.info "Set DUNE_CONTEXT_STORAGE to ironmin for the node" );
        ( ["irmin"],
          Arg.Unit
            (fun () ->
              change c ;
              c.context_storage <- Some "irmin"),
          Ezcmd.info "Set DUNE_CONTEXT_STORAGE to irmin for the node" );
        ( ["irmin2"],
          Arg.Unit
            (fun () ->
              change c ;
              c.context_storage <- Some "irmin2"),
          Ezcmd.info "Set DUNE_CONTEXT_STORAGE to irmin2 for the node" );
        ( ["context-storage"],
          Arg.String
            (fun s ->
              change c ;
              c.context_storage <- Some s),
          Ezcmd.info "STORAGE Set DUNE_CONTEXT_STORAGE to this for the node" )
      ]
  in
  (c, spec)

let print c =
  Printf.printf "Current switch: %s\n" c.current_switch ;
  Printf.printf "  node:   %s\n" c.current_node.node_name ;
  Printf.printf
    "     rpc-addr: %s:%d\n"
    c.current_node.node_rpc_addr
    c.current_node.node_rpc_port ;
  ( match c.current_node.node_local with
  | None ->
      ()
  | Some node_local ->
      Printf.printf
        "      net-addr: %s:%d\n"
        node_local.node_net_addr
        node_local.node_net_port ;
      Printf.printf "      single-process: %b\n" node_local.node_singleprocess ;
      Printf.printf
        "      context-storage: %s\n"
        node_local.node_context_storage ) ;
  Printf.printf "  client: %s\n" c.current_client.client_name ;
  Printf.printf
    "  DUNE_CONFIG= %s\n"
    (Ix_common.config_file_of_switch c.current_switch) ;
  Printf.printf "  DUNE_BINDIR= %s\n" !Ix_common.bindir ;
  Printf.printf "%!"

let set_config c config_args =
  ( match config_args.singleprocess with
  | None ->
      ()
  | Some bool ->
      (Ix_common.node_local c).node_singleprocess <- bool ;
      Printf.eprintf
        "%s.%s: single-process: %b\n%!"
        c.current_switch
        c.current_node.node_name
        bool ) ;
  match config_args.context_storage with
  | None ->
      ()
  | Some s ->
      (Ix_common.node_local c).node_context_storage <- s ;
      Printf.eprintf
        "%s.%s: context-storage: %s\n%!"
        c.current_switch
        c.current_node.node_name
        s

let commit c =
  Ix_common.save_switch_state c.current_switch c.current_switch_state ;
  ()

let action c config_args =
  if config_args.change then (set_config c config_args ; commit c) else print c

let ezcmd c =
  let (config_args, config_spec) = config_args () in
  Ix_common.ezcmd
    "config"
    ~args:config_spec
    ~doc:"Configure a resource"
    ~action:(fun () -> action c config_args)
