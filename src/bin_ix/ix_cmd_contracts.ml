(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_common
open Ix_types
open Ix_contracts.TYPES

let ( ++ ) = Int64.add

let get c file =
  Printf.eprintf "Downloading contracts into contracts.json\n%!" ;
  call
    c
    ~set_bindir
    ~cmd:"dune-client"
    ~args:
      [ "rpc";
        "get";
        "/chains/main/blocks/head/context/raw/json/contracts/index?depth=3";
        "-o";
        file ]

let gen_rows file =
  Printf.eprintf "Display accounts status\n%!" ;
  Ix_contracts.translate_to_rows file "contracts.rows"

let gen_stats file =
  Ix_contracts.with_contracts file (fun contracts ->
      let total_balance = ref 0L in
      let burnable = ref 0L in
      List.iter
        (fun (_address, c) ->
          if c.balance <> Some 900000000000L then (
            ( match c.balance with
            | None ->
                ()
            | Some balance -> (
                total_balance := !total_balance ++ balance ;
                match c.legacy2019 with
                | None ->
                    ()
                | Some true ->
                    burnable := !burnable ++ balance
                | Some false ->
                    () ) ) ;
            match c.frozen_balance with
            | None ->
                ()
            | Some list ->
                List.iter
                  (fun (_cycle, b) ->
                    let fees =
                      match b.fees with None -> 0L | Some fees -> fees
                    in
                    total_balance := !total_balance ++ b.deposits ++ fees)
                  list ))
        contracts ;
      Printf.eprintf "total_balance: %Ld\n%!" !total_balance ;
      Printf.eprintf "burnable: %Ld\n%!" !burnable ;
      ())

let ezcmd c =
  let download = ref false in
  let rows = ref false in
  let stats = ref true in
  let file = ref "contracts.json" in
  Ix_common.ezcmd
    "contracts"
    ~args:
      Ezcmd.Modules.
        [ ( ["download"],
            Arg.Set download,
            Ezcmd.info "Download the current version from the node" );
          (["rows"], Arg.Set rows, Ezcmd.info "Create file contracts.rows");
          ( ["file"],
            Arg.String (fun s -> file := s),
            Ezcmd.info "FILE.json Use FILE.json as source" ) ]
    ~action:(fun () ->
      if !download then get c !file ;
      if !rows then gen_rows !file ;
      if !stats then gen_stats !file)
    ~doc:"Download all contracts and format them as text rows"
