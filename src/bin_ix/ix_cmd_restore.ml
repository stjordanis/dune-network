(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Dune_std.OP
open Ix_types

let before = ref None

let after = ref None

let args = ref []

let snapshot = ref None

let date_of_snapshot_name snapshot =
  let open Dune_std.STRING in
  let (_network, s) = cut_at snapshot '_' in
  let (_mode, s) = cut_at s '_' in
  try
    let date = String.sub s 0 20 in
    (* Chromium replaces : by _ when saving. Reverse it. *)
    let date = Bytes.of_string date in
    Bytes.set date 13 ':' ;
    Bytes.set date 16 ':' ;
    let date = Bytes.to_string date in
    Some date
  with _ ->
    Printf.eprintf "Discarding file %s in snapshots dir\n%!" snapshot ;
    None

let find_snapshot snapshots_dir =
  Ix_common.mkdir snapshots_dir ;
  let snapshots = Sys.readdir snapshots_dir in
  Array.sort compare snapshots ;
  let snapshots = List.rev (Array.to_list snapshots) in
  let rec iter snapshots =
    match snapshots with
    | [] ->
        raise Not_found
    | snapshot :: snapshots -> (
      match date_of_snapshot_name snapshot with
      | None ->
          iter snapshots
      | Some date -> (
        match (!before, !after) with
        | (None, None) ->
            Printf.eprintf "No constraints\n%!" ;
            snapshot
        | _ ->
            Printf.eprintf
              "Checking before/after constraints on %s\n%!"
              snapshot ;
            if
              ( match !before with
              | None ->
                  true
              | Some before ->
                  date <= before )
              && match !after with None -> true | Some after -> date >= after
            then snapshot
            else iter snapshots ) )
  in
  let snapshot = iter snapshots in
  Printf.eprintf "Loading saved snapshot %s\n%!" snapshot ;
  snapshots_dir // snapshot

let command c args =
  match Ix_common.node_dir c c.current_node with
  | None ->
      Printf.eprintf "Error: not a local node\n%!" ;
      exit 2
  | Some node_dir ->
      if Sys.file_exists (node_dir // "context") then (
        Printf.eprintf
          "Error: node %S has already a storage.\n"
          c.current_node.node_name ;
        Printf.eprintf "  Use: 'ix reset' to clear node storage before\n" ;
        Printf.eprintf "%!" ;
        exit 2 )
      else
        let snapshots_dir = Ix_common.snapshots_dir c in
        let snapshot =
          match !snapshot with
          | Some snapshot ->
              snapshot
          | None -> (
            try find_snapshot snapshots_dir
            with Not_found -> (
              try
                match c.current_switch with
                | "mainnet" | "testnet" -> (
                    let url =
                      Printf.sprintf
                        "https://dune.network/files/snapshots/%s"
                        c.current_switch
                    in
                    match Ix_cmd_wget.get (url // "LAST.txt") with
                    | HttpError (_, body) | Error body ->
                        Printf.eprintf
                          "Error: could not retrieve remote snapshot:\n%s\n%!"
                          body ;
                        raise Not_found
                    | Content snapshot -> (
                        let snapshot = String.trim snapshot in
                        Printf.eprintf
                          "Downloading latest snapshot %s\n%!"
                          snapshot ;
                        match Ix_cmd_wget.get (url // snapshot) with
                        | HttpError (_, body) | Error body ->
                            Printf.eprintf
                              "Error: could not retrieve remote snapshot:\n\
                               %s\n\
                               %!"
                              body ;
                            raise Not_found
                        | Content body ->
                            let snapshot_file = snapshots_dir // snapshot in
                            let oc = open_out_bin snapshot_file in
                            output_string oc body ;
                            close_out oc ;
                            snapshot_file ) )
                | _ ->
                    raise Not_found
              with Not_found ->
                Printf.eprintf "Error: no snapshot available\n" ;
                Printf.eprintf "   Use: 'dun save' to create one\n" ;
                Printf.eprintf "%!" ;
                exit 2 ) )
        in
        Ix_common.exec
          c
          ~set_bindir
          ~cmd:"local-dune-node"
          ~args:(["snapshot"; "import"; snapshot] @ args)

(*
dun wget https://dune.network/files/snapshots/mainnet/LAST.txt
dun wget https://dune.network/files/snapshots/testnet/LAST.txt
*)

(* man: `S "section"; `P "paragraph"; `Blocks []; `Pre "code" ;
   `I ("x","y") ; `Noblank *)
let ezcmd c =
  Ix_common.ezcmd
    "restore"
    ~action:(fun () -> command c [])
    ~args:
      Ezcmd.Modules.
        [ ( [],
            Arg.Anons (fun l -> args := l),
            Ezcmd.info "ARGS passed to snapshot import" );
          ( ["snapshot"],
            Arg.String (fun s -> snapshot := Some s),
            Ezcmd.info "SNAPSHOT passed to snapshot import" );
          ( ["before"],
            Arg.String (fun s -> before := Some s),
            Ezcmd.info "Use a snapshot older than this date" );
          ( ["after"],
            Arg.String (fun s -> after := Some s),
            Ezcmd.info "Use a snapshot more recent than this date" ) ]
    ~doc:"Restore the current node from the last snapshot."
    ~man:
      [ `S "DESCRIPTION";
        `P
          {|Restore a node storage from a snapshot. The node storage must be clean, i.e. `ix reset` must have been used before. The snapshot is either the most recent one in $HOME/.dune-ix/NETWORK/snapshots/ (snapshots created by `ix save`) or it is downloaded (only for mainnet and testnet). |}
      ]
