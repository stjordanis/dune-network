(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let command () =
  Printf.printf
    "commit date: %s\n"
    Tezos_version.Current_git_info.committer_date ;
  Printf.printf "build date: %s\n" Tezos_version.Current_git_info.build_date ;
  Printf.printf "commit hash: %s\n" Tezos_version.Current_git_info.commit_hash ;
  Printf.printf "branch: %s\n" Tezos_version.Current_git_info.branch ;
  Printf.printf "%!"
