(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let pages =
  [ ("doc", "https://docs.dune.network/");
    ("edit", "https://www.liquidity-lang.org/edit");
    ("faucet", "https://faucet.dune.network/");
    ("dune", "https://dune.network");
    ("", "");
    ("twitter", "https://twitter.com/dune_network");
    ("reddit", "https://reddit.com/r/dune_network");
    ("medium", "https://medium.com/dune-network");
    ("discord", "https://discord.gg/JBUGqFg");
    ("telegram", "https://t.me/dune_network");
    ("gitlab", "https://gitlab.com/dune-network/dune-network");
    ("email", "mailto:contact@dune.network") ]

let ezcmd c =
  Ix_common.ezcmd
    "www"
    ~args:
      Ezcmd.Modules.
        [ ( ["list"],
            Arg.Unit
              (fun () ->
                Printf.printf
                  "Available pages:\n   %s\n"
                  (String.concat
                     "\n   "
                     (List.map
                        (fun (name, url) -> Printf.sprintf "%-10s %s" name url)
                        pages))),
            Ezcmd.info "List possible web pages" );
          ( [],
            Arg.Anon
              ( 0,
                fun name ->
                  match List.assoc name pages with
                  | exception Not_found ->
                      Printf.eprintf
                        "Error: unknown page %S. Use --list to check.\n%!"
                        name ;
                      exit 2
                  | url ->
                      Ix_common.call c ~cmd:"xdg-open" ~args:[url] ),
            Ezcmd.info "Open web page" ) ]
    ~doc:"Open web page solidity files"
