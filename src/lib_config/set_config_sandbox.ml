(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Config_type

let genesis_time = "2019-06-24T12:00:00Z"

let config =
  {
    Set_config_testnet.config with
    network = "Sandbox";
    (* src/bin_node/node_run_command.ml *)
    genesis_time;
    (* Use for example:
     ./dune-client dune generate genesis hash -d 2019-06-24T12:00:00Z
  *)
    genesis_block = "BLockGenesisGenesisGenesisGenesisGenesis5e671cK5Xad";
    genesis_protocol = "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
    (* src/lib_shell/distributed_db_message.ml *)
    p2p_version = "DUNE_SANDBOX_" ^ genesis_time;
    (* src/bin_node/node_config_file.ml *)
    prefix_dir = "dune-sandbox";
    bootstrap_peers = [];
    protocol_revision = Source_config.max_revision;
    (* Protocol revision of genesis *)
    minimal_fee_factor = 1;
  }

let config_option = Some config
