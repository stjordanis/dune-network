(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Delegate_utils
open Dune_daemons_args

let baker_commands () =
  let protocols = ref [] in
  let protocol_arg = protocol_arg "baker" protocols in
  let open Clic in
  let group =
    {
      Clic.name = "delegate.baker";
      title = "Commands related to the baker daemon.";
    }
  in
  [ command
      ~group
      ~desc:"Launch the unified baker daemon."
      (args9
         pidfile_arg
         max_priority_arg
         minimal_fees_arg
         minimal_nanotez_per_gas_unit_arg
         minimal_nanotez_per_byte_arg
         no_waiting_for_endorsements_arg
         lazy_switch
         lazy_mod_arg
         protocol_arg)
      ( prefixes ["run"; "with"; "local"; "node"]
      @@ param
           ~name:"context_path"
           ~desc:"Path to the node data directory (e.g. $HOME/.dune-node)"
           directory_parameter
      @@ seq_of_param
           (Client_keys.Public_key_hash.source_param
              ~name:"delegate"
              ~desc:"Delegate for which to bake") )
      (fun ( pidfile,
             max_priority,
             minimal_fees,
             minimal_nanotez_per_gas_unit,
             minimal_nanotez_per_byte,
             no_waiting_for_endorsements,
             lazy_switch,
             lazy_mod,
             _ )
           node_path
           delegates
           cctxt ->
        init_signal () ;
        may_lock_pidfile pidfile
        >>=? fun () ->
        map_p
          (fun d ->
            Client_keys.Public_key_hash.rev_find cctxt d
            >>=? function
            | Some a ->
                return a
            | None ->
                failwith
                  "Unknown delegate %s"
                  (Signature.Public_key_hash.to_b58check d))
          delegates
        >>=? fun aliases ->
        Tezos_signer_backends.Encrypted.decrypt_list cctxt aliases
        >>=? fun () ->
        let protocols = match !protocols with [] -> None | l -> Some l in
        let lazy_baker =
          match lazy_mod with
          | Some n ->
              Some n
          | None ->
              if lazy_switch then Some 0 else None
        in
        Delegate_daemons.Baker.run
          cctxt
          ~chain:cctxt#chain
          ?minimal_fees
          ?minimal_nanotez_per_gas_unit
          ?minimal_nanotez_per_byte
          ?max_priority
          ?protocols
          ?lazy_baker
          ~await_endorsements:(not no_waiting_for_endorsements)
          ~context_path:(Filename.concat node_path "context")
          delegates) ]

let endorser_commands () =
  let protocols = ref [] in
  let protocol_arg = protocol_arg "endorser" protocols in
  let open Clic in
  let group =
    {
      Clic.name = "delegate.endorser";
      title = "Commands related to the endorser daemon.";
    }
  in
  [ command
      ~group
      ~desc:"Launch the unified endorser daemon"
      (args3 pidfile_arg endorsement_delay_arg protocol_arg)
      ( prefixes ["run"]
      @@ seq_of_param
           (Client_keys.Public_key_hash.source_param
              ~name:"delegate"
              ~desc:"Delegate for which to endorse") )
      (fun (pidfile, endorsement_delay, _) delegates cctxt ->
        init_signal () ;
        may_lock_pidfile pidfile
        >>=? fun () ->
        map_p
          (fun d ->
            Client_keys.Public_key_hash.rev_find cctxt d
            >>=? function
            | Some a ->
                return a
            | None ->
                failwith
                  "Unknown delegate %s"
                  (Signature.Public_key_hash.to_b58check d))
          delegates
        >>=? fun aliases ->
        Tezos_signer_backends.Encrypted.decrypt_list cctxt aliases
        >>=? fun () ->
        let delegates_no_duplicates =
          Signature.Public_key_hash.Set.(delegates |> of_list |> elements)
        in
        ( if List.length delegates <> List.length delegates_no_duplicates then
          cctxt#message
            "Warning: the list of public key hash aliases contains duplicate \
             hashes, which are ignored"
        else Lwt.return () )
        >>= fun () ->
        let protocols = match !protocols with [] -> None | l -> Some l in
        Delegate_daemons.Endorser.run
          cctxt
          ?protocols
          ~chain:cctxt#chain
          ~delay:endorsement_delay
          delegates_no_duplicates) ]

let accuser_commands () =
  let protocols = ref [] in
  let protocol_arg = protocol_arg "accuser" protocols in
  let open Clic in
  let group =
    {
      Clic.name = "delegate.accuser";
      title = "Commands related to the accuser daemon.";
    }
  in
  [ command
      ~group
      ~desc:"Launch the unified accuser daemon"
      (args3 pidfile_arg preserved_levels_arg protocol_arg)
      (prefixes ["run"] @@ stop)
      (fun (pidfile, preserved_levels, _) cctxt ->
        init_signal () ;
        may_lock_pidfile pidfile
        >>=? fun () ->
        let protocols = match !protocols with [] -> None | l -> Some l in
        Delegate_daemons.Accuser.run
          ~chain:cctxt#chain
          ?protocols
          ~preserved_levels
          cctxt) ]
