(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)
open Client_context

let directory_parameter : (string, full) Clic.parameter =
  Clic.parameter (fun _ p ->
      if not (Sys.file_exists p && Sys.is_directory p) then
        failwith "Directory doesn't exist: '%s'" p
      else return p)

let pidfile_arg : (string option, full) Clic.arg =
  Clic.arg
    ~doc:"write process id in file"
    ~short:'P'
    ~long:"pidfile"
    ~placeholder:"filename"
    (Clic.parameter (fun _ (s : string) -> return s))

let max_priority_arg : (int option, full) Clic.arg =
  Clic.arg
    ~long:"max-priority"
    ~placeholder:"slot"
    ~doc:"maximum allowed baking slot"
    (Clic.parameter (fun _ s ->
         try return (int_of_string s)
         with _ -> failwith "Bad max priority %S" s))

let minimal_fees_arg : (string option, full) Clic.arg =
  Clic.arg
    ~long:"minimal-fees"
    ~placeholder:"amount"
    ~doc:"exclude operations with fees lower than this threshold (in dun)"
    (Clic.parameter (fun _ s -> return s))

let minimal_nanotez_per_gas_unit_arg : (Z.t option, full) Clic.arg =
  Clic.arg
    ~long:"minimal-nanodun-per-gas-unit"
    ~placeholder:"amount"
    ~doc:
      "exclude operations with fees per gas lower than this threshold (in \
       nanodun)"
    (Clic.parameter (fun _ s ->
         try return (Z.of_string s)
         with _ -> failwith "Bad  minimal fees %S" s))

let minimal_nanotez_per_byte_arg : (Z.t option, full) Clic.arg =
  Clic.arg
    ~long:"minimal-nanodun-per-byte"
    ~placeholder:"amount"
    ~doc:
      "exclude operations with fees per byte lower than this threshold (in \
       nanodun)"
    (Clic.parameter (fun _ s ->
         try return (Z.of_string s)
         with _ -> failwith "Bad minimal fees %S" s))

let no_waiting_for_endorsements_arg : (bool, full) Clic.arg =
  Clic.switch
    ~long:"no-waiting-for-late-endorsements"
    ~doc:"Disable waiting for late endorsements"
    ()

let endorsement_delay_arg : (int, full) Clic.arg =
  Clic.default_arg
    ~long:"endorsement-delay"
    ~placeholder:"seconds"
    ~doc:
      "delay before endorsing blocks\n\
       Delay between notifications of new blocks from the node and production \
       of endorsements for these blocks."
    ~default:"5"
    (Clic.parameter (fun _ s ->
         try
           let i = int_of_string s in
           fail_when (i < 0) (Exn (Failure ("bad endorsement delay " ^ s)))
           >>=? fun () -> return (int_of_string s)
         with _ -> failwith "bad endorsement delay %S" s))

let preserved_levels_arg : (int, full) Clic.arg =
  Clic.default_arg
    ~long:"preserved-levels"
    ~placeholder:"threshold"
    ~doc:"Number of effective levels kept in the accuser's memory"
    ~default:"4096"
    (Clic.parameter (fun _ s ->
         try
           let preserved_cycles = int_of_string s in
           if preserved_cycles < 0 then failwith "Bad preserved levels %S" s
           else return preserved_cycles
         with _ -> failwith "Bad preserved levels %S" s))

let add_proto p protocols =
  if not (List.exists (Protocol_hash.equal p) !protocols) then
    protocols := !protocols @ [p]

let protocol_arg name protocols =
  Clic.arg
    ~long:"protocol"
    ~short:'p'
    ~placeholder:"hash"
    ~doc:
      (Printf.sprintf
         "run %s for a specific protocol, can be used multiple times to run \
          %s on multiple protocols"
         name
         name)
    (Clic.parameter (fun _ arg ->
         try
           let (hash, _commands) =
             List.find
               (fun (hash, _commands) ->
                 String.has_prefix ~prefix:arg (Protocol_hash.to_b58check hash))
               (Client_commands.get_versions ())
           in
           add_proto hash protocols ; return_unit
         with Not_found -> fail (Client_config.Invalid_protocol_argument arg)))

let (lazy_switch : (bool, full) Clic.arg) =
  Clic.switch
    ~short:'z'
    ~long:"lazy"
    ~doc:"bake when operations are available"
    ()

let lazy_mod_arg : (int option, full) Clic.arg =
  Clic.arg
    ~long:"lazy-mod"
    ~placeholder:"confirmations"
    ~doc:"lazy mod with extra confirmation"
    (Clic.parameter (fun _ s ->
         try return (int_of_string s)
         with _ -> failwith "Bad number of confirmations %S" s))
