(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin-Labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

(* Fabrice:

   We changed the semantics of `tree`, `mem_tree` and `find_tree`:

   * In Irmin, a `tree` can be either an internal node or a blob. In
   Ironmin, we limited `tree` to only match an internal node, never a
   blob.

   * `mem` returns true if a blob is found. `mem_tree` is supposed to
   be more generic, it returns true whenever something is found, an
   internal node or a blob. In Ironmin, `mem_tree` will only return
   true if an internal node is found.

   * `find` returns a blob when it is found. `find_tree` returns
   anything that is found. In Ironmin, `find_tree` can only return an
   internal node, never a blob.


*)

type metadata = unit

type 'hash tree_hash = [`Contents of 'hash * metadata | `Node of 'hash]

module type S = sig
  val init : unit -> unit

  module Hash : sig
    type t

    val to_context_hash : t -> Context_hash.t

    val of_context_hash : Context_hash.t -> t
  end

  module Irmin : sig
    type config

    val config :
      ?mapsize:int64 ->
      ?readonly:bool ->
      ?index_log_size:int ->
      string ->
      config

    module Info : sig
      type t

      val v : date:int64 -> author:string -> string -> t

      val date : t -> int64

      val author : t -> string

      val message : t -> string
    end
  end

  module Store : sig
    (* In Ironmin, a tree is always a node. In Irmin, it can also be a
       content *)
    type tree

    type hash = Hash.t

    module Repo : sig
      type t

      val v : Irmin.config -> t Lwt.t
    end

    module Tree : sig
      val mem : tree -> string list -> bool Lwt.t

      val find : tree -> string list -> string option Lwt.t

      val remove : tree -> string list -> tree Lwt.t

      val add :
        tree -> string list -> ?metadata:metadata -> string -> tree Lwt.t

      (* [find_tree] is supposed to return anything. In Ironmin, we
         only return Some when it is a tree, while Irmin also returns
         Some when it is some content *)
      val mem_tree : tree -> string list -> bool Lwt.t

      val find_tree : tree -> string list -> tree option Lwt.t

      val add_tree : tree -> string list -> tree -> tree Lwt.t

      val list :
        tree -> string list -> (string * [`Contents | `Node]) list Lwt.t
    end

    module Commit : sig
      type t

      val of_hash : Repo.t -> Hash.t -> t option Lwt.t

      val hash : t -> Hash.t

      val info : t -> Irmin.Info.t
    end

    module Branch : sig
      val set : Repo.t -> string -> Commit.t -> unit Lwt.t

      val remove : Repo.t -> string -> unit Lwt.t

      val master : string
    end

    val repo_close : Repo.t -> unit Lwt.t

    val tree_empty : unit -> tree

    val tree_hash : Repo.t -> tree -> Hash.t tree_hash Lwt.t

    val tree_clear : ?depth:int -> tree -> unit (* unload from memory ? *)

    val commit_tree : Commit.t -> tree Lwt.t

    val commit_v :
      Repo.t ->
      info:Irmin.Info.t ->
      parents:[`Commit of Commit.t | `Hash of Hash.t] list ->
      tree ->
      Commit.t Lwt.t
  end

  val hash :
    time:Time.Protocol.t ->
    ?message:string ->
    parents:Store.Commit.t list ->
    tree:Store.tree ->
    Context_hash.t

  val gc : Store.Repo.t -> genesis:Hash.t -> current:Hash.t -> bool

  val revert : Store.Repo.t -> unit

  val storage_dir : Store.Repo.t -> string

  val clear_stats : unit -> unit

  val print_stats : Store.Repo.t -> unit

  (* functions for restoring snapshots *)
  module MEMCACHE : sig
    type t

    val create : Store.Repo.t -> (t -> 'a Lwt.t) -> 'a Lwt.t

    val add_string : t -> string -> unit Lwt.t

    val add_dir :
      t -> (string * Hash.t tree_hash) list -> Store.tree option Lwt.t
  end

  (* functions for saving snapshots *)
  val fold_tree_path :
    ?progress:(int -> unit) ->
    Store.Repo.t ->
    Store.tree ->
    ([> `Data of string | `Node of (string * Hash.t tree_hash) list] ->
    unit Lwt.t) ->
    unit Lwt.t

  val raw_commit :
    Irmin.Info.t ->
    Store.Repo.t * Store.Commit.t list * Store.tree ->
    Store.Commit.t Lwt.t

  val context_parents :
    Store.Repo.t -> Store.Commit.t -> Context_hash.t list Lwt.t

  val set_context :
    MEMCACHE.t ->
    info:Irmin.Info.t ->
    parents:Hash.t list ->
    Store.Repo.t * Store.tree ->
    Block_header.t ->
    Block_header.t option Lwt.t

  (* Further... *)
  val compute_context_hash :
    Store.Repo.t ->
    Store.tree ->
    info:Irmin.Info.t ->
    parents_hashes:Hash.t list ->
    Hash.t ->
    ( Context_hash.t
    * Store.Commit.t list
    * Store.tree
    * [`Commit of Store.Commit.t | `Hash of Hash.t] list )
    Lwt.t
end
