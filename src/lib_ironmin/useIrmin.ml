(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Irmin_legacy

module X = struct
  module IrminPath = Irmin.Path.String_list

  module Contents = struct
    type t = string

    let t =
      Irmin.Type.(like cstruct)
        (fun x -> Cstruct.to_string x)
        (fun x -> Cstruct.of_string x)

    let merge = Irmin.Merge.default Irmin.Type.(option t)

    let pp ppf s = Format.pp_print_string ppf s

    let of_string s = Ok s
  end

  module Metadata = struct
    type t = unit

    let t = Irmin.Type.unit

    let default = ()

    let merge = Irmin.Merge.default t
  end

  module IrminBlake2B : Irmin.Hash.S with type t = Context_hash.t = struct
    type t = Context_hash.t

    let digest_size = Context_hash.size

    let to_raw t = Cstruct.of_bytes (Context_hash.to_bytes t)

    let of_raw t =
      match Context_hash.of_bytes_opt (Cstruct.to_bytes t) with
      | Some t ->
          t
      | None ->
          let str = Cstruct.to_string t in
          Format.kasprintf invalid_arg "%s (%d)" str (String.length str)

    let t = Irmin.Type.like Irmin.Type.cstruct of_raw to_raw

    let digest t x =
      Context_hash.hash_bytes [Cstruct.to_bytes (Irmin.Type.encode_cstruct t x)]

    (*
    if Stores.debug_iron then
      Printf.eprintf "%S = hash_bytes %S\n%!"
        (Cstruct.to_string (to_raw hash))
        (Cstruct.to_string cstruct);
*)

    let pp = Context_hash.pp

    let of_string x =
      match Context_hash.of_b58check_exn x with
      | exception Invalid_argument s ->
          Error (`Msg s)
      | h ->
          Ok h

    let has_kind = function `SHA1 -> true | _ -> false

    let to_raw_int c =
      Int64.to_int @@ TzEndian.get_int64 (Context_hash.to_bytes c) 0
  end

  module Store =
    Irmin_lmdb.Make (Metadata) (Contents) (Irmin.Path.String_list)
      (Irmin.Branch.String)
      (IrminBlake2B)
  module P = Store.Private

  (* --- FIXME(samoht): I am so sorry --- *)
  module Hack = struct
    module StepMap = struct
      module X = struct
        type t = Store.step

        let t = Store.step_t

        let compare = Irmin.Type.compare t
      end

      include Map.Make (X)
    end

    module Contents = struct
      type key = P.Contents.key (* string *)

      type contents = P.Contents.value (* mbytes *)

      type t = Key of key | Contents of contents | Both of key * contents

      let t =
        let open Irmin.Type in
        variant "Node.Contents" (fun key contents both ->
          function
          | Key x ->
              key x
          | Contents x ->
              contents x
          | Both (x, y) ->
              both (x, y))
        |~ case1 "Key" P.Contents.Key.t (fun x -> Key x)
        |~ case1 "Contents" P.Contents.Val.t (fun x -> Contents x)
        |~ case1 "Both" (pair P.Contents.Key.t P.Contents.Val.t) (fun (x, y) ->
               Both (x, y))
        |> sealv

      let hash = function
        | Key k | Both (k, _) ->
            k
        | Contents c ->
            P.Contents.Key.digest P.Contents.Val.t c
    end

    type key = P.Node.key

    type value = [`Node of node | `Contents of Contents.t * Metadata.t]

    and map = value StepMap.t

    and node = Map of map | Key of key | Both of key * map

    let value t =
      let open Irmin.Type in
      variant "Node.value" (fun node contents ->
        function `Node x -> node x | `Contents x -> contents x)
      |~ case1 "Node" t (fun x -> `Node x)
      |~ case1 "Contents" (pair Contents.t Metadata.t) (fun x -> `Contents x)
      |> sealv

    let map value =
      let open Irmin.Type in
      let to_map x =
        List.fold_left (fun acc (k, v) -> StepMap.add k v acc) StepMap.empty x
      in
      let of_map m = StepMap.fold (fun k v acc -> (k, v) :: acc) m [] in
      like (list (pair Store.step_t value)) to_map of_map

    let node map =
      let open Irmin.Type in
      variant "Node.node" (fun map key both ->
        function Map x -> map x | Key y -> key y | Both (y, z) -> both (y, z))
      |~ case1 "Map" map (fun x -> Map x)
      |~ case1 "Key" P.Node.Key.t (fun x -> Key x)
      |~ case1 "Both" (pair P.Node.Key.t map) (fun (x, y) -> Both (x, y))
      |> sealv

    let node_t =
      Irmin.Type.mu (fun n ->
          let value = value n in
          node (map value))

    (* Mimick irmin-lmdb ordering *)
    module Sort_key = struct
      exception Result of int

      let compare (x, vx) (y, vy) =
        match (vx, vy) with
        | (`Contents _, `Contents _) ->
            String.compare x y
        | _ -> (
            let lenx = String.length x in
            let leny = String.length y in
            let i = ref 0 in
            try
              while !i < lenx && !i < leny do
                match
                  Char.compare
                    (String.unsafe_get x !i)
                    (String.unsafe_get y !i)
                with
                | 0 ->
                    incr i
                | i ->
                    raise (Result i)
              done ;
              let get len k v i =
                if i < len then String.unsafe_get k i
                else if i = len then
                  match v with `Node _ -> '/' | `Contents _ -> '\000'
                else '\000'
              in
              match Char.compare (get lenx x vx !i) (get leny y vy !i) with
              | 0 ->
                  Char.compare
                    (get lenx x vx (!i + 1))
                    (get leny y vy (!i + 1))
              | i ->
                  i
            with Result i -> i )
    end

    let sort_entries = List.fast_sort Sort_key.compare

    let pp_hex ppf x =
      let buf = IrminBlake2B.to_raw x in
      let (`Hex hex) = Hex.of_cstruct buf in
      Fmt.string ppf hex

    module Entry = struct
      type kind = [`Node | `Contents of Metadata.t]

      type entry = {kind : kind; name : string; node : IrminBlake2B.t}

      let entry_t =
        let open Irmin.Type in
        record "Tree.entry" (fun kind name node ->
            let kind =
              match kind with None -> `Node | Some m -> `Contents m
            in
            {kind; name; node})
        |+ field "kind" (option Metadata.t) (function
               | {kind = `Node; _} ->
                   None
               | {kind = `Contents m; _} ->
                   Some m)
        |+ field "name" string (fun {name; _} -> name)
        |+ field "node" IrminBlake2B.t (fun {node; _} -> node)
        |> sealr

      let of_entry e =
        ( e.name,
          match e.kind with
          | `Node ->
              `Node e.node
          | `Contents m ->
              `Contents (e.node, m) )

      let to_entry (name, value) =
        match value with
        | `Node node ->
            {name; kind = `Node; node}
        | `Contents (node, m) ->
            {name; kind = `Contents m; node}

      let t = Irmin.Type.like entry_t of_entry to_entry
    end

    let rec export_map map =
      let alist =
        StepMap.fold (fun step v acc -> (step, hash_value v) :: acc) map []
      in
      let l = sort_entries alist in
      P.Node.Val.v l

    and hash_value = function
      | `Contents (c, m) ->
          `Contents (Contents.hash c, m)
      | `Node n ->
          `Node (hash_node n)

    and hash_node = function
      | Both (k, _) | Key k ->
          k
      | Map m ->
          let v = export_map m in
          let entries = P.Node.Val.list v in
          (* This needs to match what is done in the backend... *)
          let v =
            Irmin.Type.encode_cstruct (Irmin.Type.list Entry.t) entries
          in
          IrminBlake2B.digest Irmin.Type.cstruct v

    let cast : Store.node -> node =
     fun n ->
      let buf = Irmin.Type.encode_cstruct Store.node_t n in
      match Irmin.Type.decode_cstruct node_t buf with
      | Error (`Msg e) ->
          Fmt.failwith "invalid cast\n%s" e
      | Ok x ->
          x
  end

  let tree_hash : Store.tree -> Store.Tree.hash = function
    | `Contents (c, m) ->
        `Contents (P.Contents.Key.digest P.Contents.Val.t c, m)
    | `Node n ->
        `Node (Hack.hash_node (Hack.cast n))

  let hash ~time ?(message = "") ~parents ~tree =
    let info =
      Irmin.Info.v
        ~date:(Time.Protocol.to_seconds time)
        ~author:"Tezos"
        message
    in
    let parents = List.map (fun c -> Store.Commit.hash c) parents in
    let node =
      match tree_hash tree with
      | `Contents _ ->
          assert false
      | `Node node ->
          node
    in
    let commit = P.Commit.Val.v ~parents ~node ~info in
    P.Commit.Key.digest P.Commit.Val.t commit
end

(* End module X *)

module Hash = struct
  type t = Context_hash.t

  let of_context_hash h = h

  let to_context_hash h = h
end

module Store = struct
  type hash = Hash.t

  include X.Store

  let repo_close _repo = Lwt.return ()

  let tree_empty () = Tree.empty

  let tree_hash repo c = Tree.hash repo c

  let tree_clear ?depth:_ _tree = ()

  let commit_tree commit = Commit.tree commit

  let commit_v repo ~info ~parents tree =
    let parents =
      List.map (function `Commit c -> c | _ -> assert false) parents
    in
    Commit.v repo ~info ~parents tree
end

let current_dir = ref "test-db.irmin"

module Irmin = struct
  module Info = Irmin.Info

  type config = Irmin.config

  let config ?mapsize ?readonly ?index_log_size:_ dir =
    (let maybe_mapfile = Filename.concat dir "mapfile.bin" in
     if Sys.file_exists maybe_mapfile then (
       Printf.eprintf
         "Storage error: file %S exists, showing that you started your node \
          with Ironmin, but you are now running in Irmin mode. Use \
          DUNE_CONTEXT_STORAGE=ironmin to start your node in Ironmin mode. If \
          you really want to switch back to Irmin, use the \
          /storage/context/revert RPC to convert the database back to Irmin \
          format.\n\
          %!"
         maybe_mapfile ;
       exit 2 )) ;
    current_dir := dir ;
    Irmin_lmdb.config ?mapsize ?readonly dir
end

let hash = X.hash

let gc _repo ~genesis:_ ~current:_ = false

let revert _repo = ()

let clear_stats () = ()

let print_stats _repo = ()

let init () = ()

let storage_dir _repo = !current_dir

module MEMCACHE = struct
  type t = {repo : Store.Repo.t}

  let create repo f = f {repo}

  let add_string batch string =
    let tree = Store.Tree.of_contents string in
    Store.Tree.hash batch.repo tree >|= fun _hash -> ()

  let add_tree batch tree = Store.Tree.hash batch.repo tree >|= fun _hash -> ()

  (* add_node in Irmin2 *)
  let add_hash batch tree key hash =
    Store.Tree.of_hash batch.repo hash
    >>= function
    | None ->
        Lwt.return_none
    | Some sub_tree ->
        Store.Tree.add_tree tree key sub_tree >>= Lwt.return_some

  let add_dir batch l =
    let rec fold_list sub_tree = function
      | [] ->
          Lwt.return_some sub_tree
      | (step, hash) :: tl -> (
          add_hash batch sub_tree [step] hash
          >>= function
          | None -> Lwt.return_none | Some sub_tree -> fold_list sub_tree tl )
    in
    fold_list (Store.tree_empty ()) l
    >>= function
    | None ->
        Lwt.return_none
    | Some tree ->
        add_tree batch tree >|= fun () -> Some tree

  let set_context batch ~info ~parents ~node =
    let v = Store.Private.Commit.Val.v ~info ~node ~parents in
    Store.Private.Commit.add (Store.Private.Repo.commit_t batch.repo) v
end

module I = struct
  let tree_hash repo = function
    | `Node _ as node ->
        Store.Tree.hash repo node
    | contents ->
        Lwt.return (X.tree_hash contents)

  let sub_tree tree key = Store.Tree.find_tree tree key

  let tree_list tree = Store.Tree.list tree []

  let tree_content tree = Store.Tree.find tree []
end

let fold_tree_path ?(progress = fun _ -> ()) repo tree f =
  (* Noting the visited hashes *)
  let visited_hash = Hashtbl.create 1000 in
  let visited h = Hashtbl.mem visited_hash h in
  let set_visit h = Hashtbl.add visited_hash h () in
  let cpt = ref 0 in
  let rec fold_tree_path tree =
    I.tree_list tree
    >>= fun keys ->
    let keys = List.sort (fun (a, _) (b, _) -> String.compare a b) keys in
    Lwt_list.map_s
      (fun (name, kind) ->
        I.sub_tree tree [name]
        >>= function
        | None ->
            assert false
        | Some sub_tree ->
            I.tree_hash repo sub_tree
            >>= fun hash ->
            ( if visited hash then Lwt.return_unit
            else (
              progress !cpt ;
              incr cpt ;
              set_visit hash ;
              (* There cannot be a cycle *)
              match kind with
              | `Node ->
                  fold_tree_path sub_tree
              | `Contents -> (
                  I.tree_content sub_tree
                  >>= function
                  | None -> assert false | Some data -> f (`Data data) ) ) )
            >|= fun () -> (name, hash))
      keys
    >>= fun sub_keys -> f (`Node sub_keys)
  in
  fold_tree_path tree

let raw_commit info (repo, parents, tree) =
  let parents = List.map (fun c -> `Commit c) parents in
  Store.commit_v repo ~info ~parents tree

let context_parents repo commit =
  (* XXX(samoht): fixed in irmin v2 *)
  let key = Store.Commit.hash commit in
  Store.Private.Commit.find (Store.Private.Repo.commit_t repo) key
  >|= fun v ->
  let commit = match v with None -> assert false | Some v -> v in
  let parents = Store.Private.Commit.Val.parents commit in
  List.sort Context_hash.compare parents

let set_context batch ~info ~parents (repo, tree) bh =
  Store.tree_hash repo tree
  >>= function
  | `Node node ->
      MEMCACHE.set_context batch ~info ~node ~parents
      >>= fun ctxt_h ->
      if
        Context_hash.equal
          bh.Block_header.shell.context
          (Hash.to_context_hash ctxt_h)
      then Lwt.return_some bh
      else Lwt.return_none
  | `Contents _ ->
      assert false

(* Mock some Store types, so we can build our own Merkle tree. *)

module Mock : sig
  val node : Store.Repo.t -> X.P.Node.key -> Store.node

  val commit : Store.repo -> X.Hack.key -> X.P.Commit.value -> Store.commit
end = struct
  [@@@ocaml.warning "-37"]

  type commit = {r : Store.Repo.t; h : Context_hash.t; v : X.P.Commit.value}

  type empty

  open X

  type u =
    | Map : empty -> u
    | Key : Store.Repo.t * P.Node.key -> u
    | Both : empty * empty * empty -> u

  and node = {mutable v : u}

  let node repo key =
    let t : u = Key (repo, key) in
    let node : node = {v = t} in
    (Obj.magic node : Store.node)

  (* Fabrice: What the fuck !!! *)

  let commit r h v =
    let c : commit = {r; h; v} in
    (Obj.magic c : Store.commit)

  (* Fabrice: What the fuck !!! *)
end

let compute_context_hash repo tree ~info ~parents_hashes data_hash =
  let open X in
  let o_tree =
    Hack.cast (match tree with `Node n -> n | _ -> assert false)
  in
  let map = match o_tree with Map m -> m | _ -> assert false in
  let data_tree = Hack.Key data_hash in
  let new_map = Hack.Map (Hack.StepMap.add "data" (`Node data_tree) map) in
  let node = Hack.hash_node new_map in
  let commit = P.Commit.Val.v ~parents:parents_hashes ~node ~info in
  let computed_context_hash = P.Commit.Key.digest P.Commit.Val.t commit in
  let mock_parents =
    List.map (fun h -> Mock.commit repo h commit) parents_hashes
  in
  let commit_parents = List.map (fun c -> `Commit c) mock_parents in
  Lwt.return
    ( computed_context_hash,
      mock_parents,
      `Node (Mock.node repo data_hash),
      commit_parents )
