(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin-Labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

module Hash = struct
  type t = Context_hash.t

  let of_context_hash h = h

  let to_context_hash h = h

  let to_string h = Context_hash.to_string h

  let of_string h =
    match Context_hash.of_string_opt h with
    | None ->
        assert false
    | Some v ->
        v

  let hash_string l =
    let hash = Context_hash.hash_string l in
    (* if debug_iron then Printf.eprintf "%S = hash_iron %S\n%!"
       (to_string hash) (String.concat "" l); *)
    hash
end

module Storage = Ocplib_ironmin.Ironmin_lmdb

module X = Ocplib_ironmin.Ironmin.Make (struct
  module Hash = Hash
  module Storage = Storage
  module Key = Storage_keys

  type config = Storage.config

  let create_storage = Storage.create_storage
end)

module Store = struct
  type hash = Hash.t

  include X

  let repo_close repo = Repo.close repo

  let tree_empty () = Tree.empty ()

  let tree_hash repo tree = Tree.hash repo tree

  let tree_clear ?depth tree = Tree.clear ?depth tree

  let tree_unshallow repo tree = Tree.unshallow repo tree

  let commit_tree commit = Commit.tree commit

  let commit_v repo ~info ~parents tree =
    let parents =
      List.map (function `Hash h -> h | _ -> assert false) parents
    in
    Commit.v repo ~info ~parents tree
end

module Irmin = struct
  module Info = Store.Info

  type config = Store.config

  let config ?mapsize ?readonly ?index_log_size:_ dir =
    Storage.create_config ?mapsize ?readonly dir
end

let hash ~time ?(message = "") ~parents ~tree =
  Store.Commit.to_hash
    ~info:
      (Store.Info.v
         ~date:(Time.Protocol.to_seconds time)
         ~author:"Tezos"
         message)
    ~parents
    tree

let init () = Store.init ()

let raw_commit info (repo, parents, tree) =
  let parents = List.map Store.Commit.hash parents in
  let parents = List.map (fun h -> `Hash h) parents in
  Store.Tree.unshallow repo tree
  >>= fun () ->
  Store.commit_v repo ~info ~parents tree
  >|= fun h -> Store.Tree.clear tree ; h

let set_context _batch ~info ~parents (repo, tree) bh =
  let parents = List.map (fun h -> `Hash h) parents in
  Store.commit_v repo ~info ~parents tree
  >>= fun c ->
  let ctxt_h = Store.Commit.hash c in
  if
    Context_hash.equal
      bh.Block_header.shell.context
      (Hash.to_context_hash ctxt_h)
  then Lwt.return_some bh
  else Lwt.return_none

include Store.TOPLEVEL
