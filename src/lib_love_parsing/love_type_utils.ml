(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types
open Love_ast_types.TYPE

let rec isComparable (user_comp : user_comp) t =
  let isComparable = isComparable user_comp in
  match t with
  | TPackedStructure _ | TContractInstance _ ->
      false
  | TTuple l ->
      List.for_all isComparable l
  | TUser (n, l) ->
      List.for_all isComparable l && user_comp n
  | TArrow _ ->
      false
  | TVar {tv_traits; _} ->
      tv_traits.tcomparable
  | TForall _ ->
      false

(* Could it be made comparable ? I.e empty list ?*)

let rec replace_struct_sig (user_comp : user_comp) tvarmap (s : structure_sig)
    : structure_sig =
  {
    sig_content = List.map (replace_content user_comp tvarmap) s.sig_content;
    sig_kind = s.sig_kind;
  }

and replace_content (user_comp : user_comp) tvarmap (s, c) =
  let new_c =
    match c with
    | SType t ->
        SType (replace_sig_type user_comp tvarmap t)
    | SException tl ->
        SException (List.map (replace_map user_comp tvarmap) tl)
    | SInit t ->
        SInit (replace_map user_comp tvarmap t)
    | SEntry tp ->
        SEntry (replace_map user_comp tvarmap tp)
    | SView t ->
        SView (replace_map user_comp tvarmap t)
    | SValue t ->
        SValue (replace_map user_comp tvarmap t)
    | SStructure (Named _) ->
        c
    | SStructure (Anonymous s) ->
        SStructure (Anonymous (replace_struct_sig user_comp tvarmap s))
    | SSignature s ->
        SSignature (replace_struct_sig user_comp tvarmap s)
  in
  (s, new_c)

and replace_tdef (user_comp : user_comp) tvarmap td : typedef =
  match td with
  | SumType {sparams; scons; srec} ->
      let tvarmap =
        List.fold_left
          (fun map param -> TypeVarMap.remove param map)
          tvarmap
          sparams
      in
      let scons =
        List.map
          (fun (cstr, l) -> (cstr, List.map (replace_map user_comp tvarmap) l))
          scons
      in
      SumType {sparams; scons; srec}
  | RecordType {rparams; rfields; rrec} ->
      let tvarmap =
        List.fold_left
          (fun map param -> TypeVarMap.remove param map)
          tvarmap
          rparams
      in
      let rfields =
        List.map
          (fun (field, t) -> (field, replace_map user_comp tvarmap t))
          rfields
      in
      RecordType {rparams; rfields; rrec}
  | Alias {aparams; atype} ->
      let tvarmap =
        List.fold_left
          (fun map param -> TypeVarMap.remove param map)
          tvarmap
          aparams
      in
      Alias {aparams; atype = replace_map user_comp tvarmap atype}

(*| Abstract {absparams; absname; abstdef = None} ->
    td
  | Abstract {absparams; absname; abstdef = Some tdef} ->
    let tvarmap =
      List.fold_left
        (fun map param -> TypeVarMap.remove param map)
        tvarmap
        absparams
    in
    Abstract {absparams; absname; abstdef = Some (replace_tdef tvarmap tdef)} *)
and replace_sig_type (user_comp : user_comp) tvarmap s =
  match s with
  | SAbstract _ ->
      s
  (*| SInternal t -> SInternal (replace_tdef tvarmap t)*)
  | SPrivate t ->
      SPrivate (replace_tdef user_comp tvarmap t)
  | SPublic t ->
      SPublic (replace_tdef user_comp tvarmap t)

and replace_map (user_comp : user_comp) tvarmap t =
  let replace = replace_map user_comp tvarmap in
  match t with
  | TVar v -> (
    match TypeVarMap.find_opt v tvarmap with
    | None ->
        t
    | Some new_t ->
        if v.tv_traits.tcomparable && not (isComparable user_comp new_t) then
          (*
          Printf.eprintf
            "[replace_map] Error : trying to replace %a with %a : incompatible traits@."
            pretty t pretty new_t;
*)
          raise
            (Love_ast_types.BadArgument
               "Replacing types with incomaptible traits")
        else new_t )
  | TPackedStructure (Named _) | TContractInstance (Named _) ->
      t
  | TPackedStructure (Anonymous s) ->
      TPackedStructure (Anonymous (replace_struct_sig user_comp tvarmap s))
  | TContractInstance (Anonymous s) ->
      TContractInstance (Anonymous (replace_struct_sig user_comp tvarmap s))
  | TUser (n, args) ->
      TUser (n, List.map replace args)
  | TTuple l ->
      TTuple (List.map replace l)
  | TArrow (t, t') ->
      TArrow (replace t, replace t')
  | TForall (v, t) as typ -> (
    match TypeVarMap.find_opt v tvarmap with
    | None ->
        TForall (v, replace t)
    | Some _ ->
        typ )

let ( @=> ) t1 t2 = TArrow (t1, t2)

let arrowFromList l =
  let rec loop = function
    | [t] ->
        t
    | hd :: tl ->
        hd @=> loop tl
    | _ ->
        assert false
  in
  match l with [] -> raise (BadArgument "arrowFromList") | l -> loop l
