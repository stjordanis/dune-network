(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Node_manager_state
open Node_manager_encodings
module Public_key_hash = Signature.Public_key_hash

let register_client_dir client_dir =
  with_state_lock
  @@ fun () ->
  global_state.client_dir <- Some client_dir ;
  write_state ()

let register_accounts accounts =
  iter_p
    (fun (alias, _) ->
      match Public_key_hash.of_b58check_opt alias with
      | None ->
          return_unit
      | Some _ ->
          failwith "Forbidden alias %s (it is a public key hash)" alias)
    accounts
  >>=? fun () ->
  with_state_lock
  @@ fun () ->
  let accounts =
    List.fold_left
      (fun acc (alias, pkh) -> String.Map.add alias pkh acc)
      global_state.accounts
      accounts
  in
  global_state.accounts <- accounts ;
  write_state ()

let get_accounts () = global_state.accounts

let get_account name = String.Map.find name global_state.accounts
