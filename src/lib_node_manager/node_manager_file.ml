(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* include Internal_event.Legacy_logging.Make_semantic
 *     (struct let name = "node.manager.file" end) *)

let mutex = Lwt_mutex.create ()

class type t =
  object
    method base_dir : string

    method read_file : string -> string tzresult Lwt.t

    method with_lock : (unit -> 'a Lwt.t) -> 'a Lwt.t

    method load : default:'a -> 'a Data_encoding.encoding -> 'a tzresult Lwt.t

    method write : 'a -> 'a Data_encoding.encoding -> unit tzresult Lwt.t
  end

class manager ~base_dir name : t =
  object (self)
    method base_dir = base_dir

    method read_file path =
      Lwt.catch
        (fun () ->
          Lwt_io.(with_file ~mode:Input path read)
          >>= fun content -> return content)
        (fun exn -> failwith "cannot read file (%s)" (Printexc.to_string exn))

    method private filename =
      Filename.concat
        base_dir
        Str.(global_replace (regexp_string " ") "_" name)

    method with_lock : type a. (unit -> a Lwt.t) -> a Lwt.t =
      fun f ->
        Lwt_mutex.with_lock mutex
        @@ fun () ->
        let unlock fd =
          let fd = Lwt_unix.unix_file_descr fd in
          Unix.lockf fd Unix.F_ULOCK 0 ;
          Unix.close fd
        in
        let lock () =
          Lwt_unix.openfile
            (Filename.concat base_dir "manager_lock")
            Lwt_unix.[O_CREAT; O_WRONLY]
            0o644
          >>= fun fd ->
          Lwt_unix.lockf fd Unix.F_LOCK 0
          >>= fun () ->
          Lwt.return
            ( fd,
              Lwt_unix.on_signal Sys.sigint (fun _s ->
                  unlock fd ; exit 0 (* exit code? *)) )
        in
        lock ()
        >>= fun (fd, sh) ->
        (* catch might be useless if f always uses the error monad *)
        Lwt.catch f (function e -> Lwt.return (unlock fd ; raise e))
        >>= fun res ->
        Lwt.return (unlock fd)
        >>= fun () ->
        Lwt_unix.disable_signal_handler sh ;
        Lwt.return res

    method load : type a.
        default:a -> a Data_encoding.encoding -> a tzresult Lwt.t =
      fun ~default encoding ->
        let filename = self#filename in
        if not (Sys.file_exists filename) then return default
        else
          Lwt_utils_unix.Json.read_file filename
          |> generic_trace "could not read the manager file"
          >>=? fun json ->
          match Data_encoding.Json.destruct encoding json with
          | exception e ->
              failwith
                "did not understand the file %s : %s"
                filename
                (Printexc.to_string e)
          | data ->
              return data

    method write : type a. a -> a Data_encoding.encoding -> unit tzresult Lwt.t
        =
      fun list encoding ->
        Lwt.catch
          (fun () ->
            Lwt_utils_unix.create_dir base_dir
            >>= fun () ->
            let filename = self#filename in
            let json = Data_encoding.Json.construct encoding list in
            Lwt_utils_unix.Json.write_file filename json)
          (fun exn -> Lwt.return (error_exn exn))
        |> generic_trace "could not write the %s file." name
  end
