(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(*
   The new operation that we want to add is along
   Activate_protocol_parameters { level ; protocol_parameters }
   We could also change the protocol using `Updater.activate`.

   Also, the following operations do not work on Dune:
   * Proposals
   * Ballot
   They are disabled in apply.ml
*)


(* If you add a new operation here, you will need:
   * to define a new encoding case in this file for the new operation
   * to define a result for the execution of the new operation, in the file
      `lib_protocol/dune_apply_result.ml`, together with its encoding.
      Also, the encodings for operations are re-implemented there.
   * to modify `lib_protocol/dune_apply.ml` to execute the new operation,
     and return its apply result
   * to modify `lib_client/dune_operation_result.ml` to print the
     new operation result.
*)

type dune_manager_operation =
  | Dune_activate_protocol of
      { level : int32 ;
        protocol : Protocol_hash.t option ;
        protocol_parameters : Dune_parameters_repr.parameters option }

  | Dune_manage_accounts of MBytes.t

(* These operations are included in:
   | Manager_operation : {
      source: Contract_repr.contract ;
      fee: Tez_repr.tez ;
      counter: counter ;
      operation: 'kind manager_operation ;
      gas_limit: Z.t;
      storage_limit: Z.t;
    }
*)

open Data_encoding

let case tag name args proj inj =
  let open Data_encoding in
  case tag
    ~title:(String.capitalize_ascii name)
    (merge_objs
       (obj1 (req "dune_kind" (constant name)))
       args)
    (fun x -> match proj x with None -> None | Some x -> Some ((), x))
    (fun ((), x) -> inj x)

let activate_protocol_case =
  case
    (Tag 0)
    "dune_activate_protocol"
    (obj3
       (req "level" int32)
       (opt "protocol" Protocol_hash.encoding)
       (opt "protocol_parameters" Dune_parameters_repr.parameters_encoding))
    (function
      | Dune_activate_protocol { level ; protocol ; protocol_parameters } ->
          Some ( level, protocol, protocol_parameters )
      | _ -> None
    )
    (function (level , protocol , protocol_parameters ) ->
       Dune_activate_protocol { level ; protocol ; protocol_parameters })

let manage_accounts_case =
  case
    (Tag 1)
    "dune_manage_accounts"
    (obj1 (req "bytes" (dynamic_size Variable.bytes)))
    (function
      | Dune_manage_accounts bytes -> Some bytes
      | _ -> None
    )
    (function bytes -> Dune_manage_accounts bytes)

let encoding =
  union
    ~tag_size:`Uint8 [

    activate_protocol_case ;
    manage_accounts_case ;
  ]
