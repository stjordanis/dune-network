(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type dune_manager_operation_result =
  | Dune_activate_protocol_result
  | Dune_manage_accounts_result
  | Dune_manage_account_result
  | Dune_clear_delegations_result of int

open Data_encoding

let case tag name args proj inj =
  let open Data_encoding in
  case tag
    ~title:(String.capitalize_ascii name)
    (merge_objs
       (obj1 (req "kind" (constant name)))
       args)
    (fun x -> match proj x with None -> None | Some x -> Some ((), x))
    (fun ((), x) -> inj x)

let activate_protocol_result_case =
  case
    (Tag 0)
    "activate_protocol"
    Data_encoding.empty
    (function Dune_activate_protocol_result -> Some () | _ -> None)
    (fun () -> Dune_activate_protocol_result)

let manage_accounts_result_case =
  case
    (Tag 1)
    "manage_accounts"
    Data_encoding.empty
    (function Dune_manage_accounts_result -> Some () | _ -> None)
    (fun () -> Dune_manage_accounts_result)

let manage_account_result_case =
  case
    (Tag 2)
    "manage_account"
    Data_encoding.empty
    (function Dune_manage_account_result -> Some () | _ -> None)
    (fun () -> Dune_manage_account_result)

let clear_delegations_result_case =
  case
    (Tag 3)
    "clear_delegations"
    (obj1
       (req "undelegated" int31))
    (function Dune_clear_delegations_result n -> Some n | _ -> None)
    (fun n -> Dune_clear_delegations_result n)

let encoding =
  union
    ~tag_size:`Uint8 [
    activate_protocol_result_case ;
    manage_accounts_result_case ;
    manage_account_result_case ;
    clear_delegations_result_case ;
  ]
