(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context

val normalize_script :
  Alpha_context.t ->
  Contract.t ->
  Script.t ->
  (Script.canonical * context) tzresult Lwt.t

val denormalize_script :
  Alpha_context.t ->
  Script.canonical ->
  (Script.t * context) tzresult Lwt.t

val readable_storage :
  Alpha_context.t ->
  code:Script.lazy_expr ->
  storage:Script.lazy_expr ->
  Script.expr tzresult Lwt.t

val readable_script :
  Alpha_context.t ->
  Script.canonical ->
  Script.t tzresult Lwt.t

val typecheck_code :
  Alpha_context.t ->
  Dune_lang_repr.expr ->
  (Script_tc_errors.type_map * Alpha_context.t) tzresult Lwt.t

val typecheck_data :
  Alpha_context.t ->
  Dune_lang_repr.expr * Dune_lang_repr.expr ->
  Alpha_context.t tzresult Lwt.t

val typecheck :
  ?type_only:bool ->
  Alpha_context.t ->
  Script_interpreter.step_constants ->
  Script.t ->
  ((Script.canonical * Contract.big_map_diff option) * context) tzresult Lwt.t

val execute :
  Alpha_context.t ->
  Script_ir_translator.unparsing_mode ->
  Script_interpreter.step_constants ->
  entrypoint:string ->
  code:Script.expr ->
  storage:Script.expr ->
  parameter:Script.expr ->
  apply_manager_operation_content:
    (Alpha_context.t -> Script_ir_translator.unparsing_mode ->
     payer:Contract.t -> source:Contract.t ->
     chain_id:Chain_id.t ->
     internal:bool ->
     'kind manager_operation ->
     (context *
      'kind Apply_results.successful_manager_operation_result *
      packed_internal_operation list) tzresult Lwt.t) ->
  Script_interpreter.execution_result tzresult Lwt.t

val trace:
  Alpha_context.t ->
  Script_ir_translator.unparsing_mode ->
  Script_interpreter.step_constants ->
  entrypoint:string ->
  code: Script.expr ->
  storage: Script.expr ->
  parameter: Script.expr ->
  (Script_interpreter.execution_result *
   Script_interpreter.execution_trace) tzresult Lwt.t

val execute_fee_script :
  Alpha_context.t ->
  Script_interpreter.step_constants ->
  entrypoint:string ->
  fee_code:Script.expr ->
  storage:Script.expr ->
  parameter:Dune_lang_repr.expr ->
  Script_interpreter.fee_execution_result tzresult Lwt.t

val execute_value :
  Alpha_context.t ->
  self:Contract_repr.t ->
  val_name:string ->
  code:Script.expr ->
  storage:Script.expr ->
  parameter:Dune_lang_repr.expr option ->
  Script_interpreter.val_execution_result tzresult Lwt.t

val find_entrypoint:
  Alpha_context.t ->
  bool ->
  code: Script.expr ->
  entrypoint: string ->
  Script.expr tzresult Lwt.t

val list_entrypoints:
  Alpha_context.t ->
  bool ->
  code: Script.expr ->
  (Michelson_v1_primitives.prim list list *
   ((string * Script.expr) list)) tzresult Lwt.t

val pack_data :
  Alpha_context.t ->
  data:Script.expr ->
  typ_opt:Script.expr option ->
  (MBytes.t * Alpha_context.t) tzresult Lwt.t

val unpack_data :
  Alpha_context.t ->
  data:MBytes.t ->
  typ:Script.expr ->
  (Script.expr * Alpha_context.t) option tzresult Lwt.t

val big_map_get_opt :
  Alpha_context.t ->
  id:counter ->
  key:Script.expr ->
  Script.expr option tzresult Lwt.t
