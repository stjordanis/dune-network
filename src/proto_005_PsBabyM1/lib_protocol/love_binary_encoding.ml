(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_binary_processor
open Data_encoding

module Type = struct
  let encoding =
    conv Serializer.Type.serialize
      Deserializer.Type.deserialize
      Data_encoding.bytes
end

module Value = struct
  let encoding =
    conv Serializer.Value.serialize
      Deserializer.Value.deserialize
      Data_encoding.bytes

  let live_structure_encoding =
    conv Serializer.Value.serialize_live_structure
      Deserializer.Value.deserialize_live_structure
      Data_encoding.bytes

  let live_contract_encoding =
    conv Serializer.Value.serialize_live_contract
      Deserializer.Value.deserialize_live_contract
      Data_encoding.bytes

let fee_code_encoding =
  conv Serializer.Value.serialize_fee_code
    Deserializer.Value.deserialize_fee_code
    Data_encoding.bytes
end

module Ast = struct
  let exp_encoding =
    conv Serializer.Located.serialize_exp
      Deserializer.Located.deserialize_exp
      Data_encoding.bytes
  let const_encoding =
    conv Serializer.Located.serialize_const
      Deserializer.Located.deserialize_const
      Data_encoding.bytes
  let structure_encoding =
    conv Serializer.Located.serialize_structure
      Deserializer.Located.deserialize_structure
      Data_encoding.bytes
  let top_contract_encoding =
    conv Serializer.Located.serialize_top_contract
      Deserializer.Located.deserialize_top_contract
      Data_encoding.bytes
end

module RuntimeAst = struct
  let exp_encoding =
    conv Serializer.Runtime.serialize_exp
      Deserializer.Runtime.deserialize_exp
      Data_encoding.bytes
  let const_encoding =
    conv Serializer.Runtime.serialize_const
      Deserializer.Runtime.deserialize_const
      Data_encoding.bytes
  let structure_encoding =
    conv Serializer.Runtime.serialize_structure
      Deserializer.Runtime.deserialize_structure
      Data_encoding.bytes
  let top_contract_encoding =
    conv Serializer.Runtime.serialize_top_contract
      Deserializer.Runtime.deserialize_top_contract
      Data_encoding.bytes
end
