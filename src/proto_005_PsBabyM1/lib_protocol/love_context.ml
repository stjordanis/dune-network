(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Love_pervasives
open Love_pervasives.Exceptions
open Love_value
open Love_ast_types

module ContractMap = Map.Make (Contract)

type contract_script =
  | ScriptMich of { code: Script_repr.expr; storage: Script_repr.expr }
  | ScriptLove of { code: LiveContract.t; storage: Value.t }

type contract_code =
  | CodeMich of Script_repr.expr
  | CodeLove of LiveContract.t

type contract_storage =
  | StoreMich of Script_repr.expr
  | StoreLove of Value.t

type contract_contents =
  | CntMichelson of string * TYPE.t
  | CntLove of LiveStructure.content

type cache_data = contract_code option * contract_storage option

type t = {
  (* The Dune protocol context *)
  actxt: Alpha_context.t;

  (* Constants for the current execution *)
  self: Contract.t;
  source: Contract.t;
  payer: Contract.t;
  amount: Tez.t;

  (* The current contract's type environment *)
  type_env: Love_tenv.t;

  (* Contract cache, persist accross internal transactions *)
  contracts: cache_data ContractMap.t;
}

(* Not tied to this module, but put here for convenience *)
type val_or_exp =
  | Exp of Love_runtime_ast.param list
  | Val of Love_value.Value.t list

exception UserException of
    t * (Love_runtime_ast.exn_name * Value.t list) * AST.location option

let dummy_address =
  Contract.implicit_contract (Signature.Public_key_hash.zero)

let init
    ?(self=dummy_address) ?(source=dummy_address)
    ?(payer=dummy_address) ?(amount=Tez.zero)
    ?(code=LiveContract.unit_void) ?(storage)
    ?(type_env=Love_tenv.empty (Contract []) ()) actxt
  =
  let code = Some (CodeLove code) in
  let storage = match storage with
    | None -> None
    | Some v -> Some (StoreLove v)
  in
  let contracts = ContractMap.singleton self (code, storage) in
  { actxt; self; source; payer; amount; type_env; contracts }

let consume_gas (ctxt : t) (gas : Alpha_context.Gas.cost) : t tzresult Lwt.t =
  Lwt.return (Gas.consume ctxt.actxt gas >|? fun actxt -> { ctxt with actxt })

let set_type_env ctxt type_env = { ctxt with type_env }

let update_code ctxt addr (code : contract_code) =
  let storage = match ContractMap.find_opt addr ctxt.contracts with
    | Some (_, Some storage) -> Some storage
    | Some (_, None) -> None
    | None -> None
  in
  { ctxt with
    contracts = ContractMap.add addr (Some code, storage) ctxt.contracts }

let update_storage ctxt addr (storage : contract_storage) =
  let code = match ContractMap.find_opt addr ctxt.contracts with
    | Some (Some code, _) -> Some code
    | Some (None, _) -> None
    | None -> None
  in
  { ctxt with
    contracts = ContractMap.add addr (code, Some storage) ctxt.contracts }

let forget_storage ctxt addr =
  match ContractMap.find_opt addr ctxt.contracts with
  | Some (code, _) ->
     { ctxt with contracts = ContractMap.add addr (code, None) ctxt.contracts }
  | None -> ctxt

let unset_script ctxt addr =
  { ctxt with contracts = ContractMap.remove addr ctxt.contracts }

let get_code ctxt addr =
  match ContractMap.find_opt addr ctxt.contracts with
  | Some (Some c, _) -> return (ctxt, Some c)
  | Some (None, _)
  | None ->
      Contract.exists ctxt.actxt addr
      >>=? function
      | false -> return (ctxt, None)
      | true ->
          Contract.get_script_code ctxt.actxt addr
          >>=? fun (actxt, code) ->
          let ctxt = { ctxt with actxt } in
          match code with (* Can only fail because of gas *)
          | None ->
              let unit =
                if Love_pervasives.has_protocol_revision 3 then
                  LiveContract.unit_default ()
                else
                  LiveContract.unit_void in
              return (ctxt, Some (CodeLove unit))
          | Some code ->
              Script.force_decode ctxt.actxt code
              >>=? fun (code, actxt) ->
              let ctxt = { ctxt with actxt } in
              let code = match code with
                | Michelson_expr code -> Some (CodeMich code)
                | Dune_expr _ -> None
                | Dune_code code ->
                    match Love_repr.is_code code with
                    | Some (LiveContract code) -> Some (CodeLove code)
                    | Some (Contract _ | FeeCode _)
                    | None -> None
              in
              match code with
              | None -> return (ctxt, None)
              | Some code ->
                  let ctxt = update_code ctxt addr code in
                  return (ctxt, Some code)

let get_storage ctxt addr =
  match ContractMap.find_opt addr ctxt.contracts with
  | Some (_, Some s) -> return (ctxt, Some s)
  | Some (_, None)
  | None ->
      Contract.exists ctxt.actxt addr
      >>=? function
      | false -> return (ctxt, None)
      | true ->
          Contract.get_storage ctxt.actxt addr
          >>=? fun (actxt, storage) ->
          let ctxt = { ctxt with actxt } in
          match storage with (* Can only fail because of gas *)
          | None -> return (ctxt, Some (StoreLove Value.VUnit))
          | Some storage ->
              Script.force_decode ctxt.actxt storage
              >>=? fun (storage, actxt) ->
              let ctxt = { ctxt with actxt } in
              let storage = match storage with
                | Michelson_expr storage -> Some (StoreMich storage)
                | Dune_code _ -> None
                | Dune_expr storage ->
                    match Love_repr.is_const storage with
                    | Some (Value storage) -> Some (StoreLove storage)
                    | Some (Type _)
                    | None -> None
              in
              match storage with
              | None -> return (ctxt, None)
              | Some storage ->
                  let ctxt = update_storage ctxt addr storage in
                  return (ctxt, Some storage)

let get_script ctxt addr =
  get_code ctxt addr >>=? fun (ctxt, code) ->
  get_storage ctxt addr >>=? fun (ctxt, storage) ->
  let script = match code, storage with
    | Some (CodeMich code), Some (StoreMich storage) ->
        Some (ScriptMich { code; storage })
    | Some (CodeLove code), Some (StoreLove storage) ->
        Some (ScriptLove { code; storage })
    | _ -> None
  in
  return (ctxt, script)

let resolve_path ctxt kt1 id_opt =
  get_code ctxt kt1 >>=? function
  | _, None ->
      raise (InvariantBroken (
          Format.asprintf "Contract %a not found" Contract.pp kt1))
  | ctxt, Some (CodeLove { root_struct = code; version = _ }) ->
      begin
        match id_opt with
        | None ->
            return (ctxt, Some (CntLove (LiveStructure.VStructure code)))
        | Some id ->
            match LiveStructure.resolve_id_in_struct code id with
            | None -> return (ctxt, None)
            | Some cnt -> return (ctxt, Some (CntLove cnt))
      end
  | ctxt, Some (CodeMich code) ->
      begin
        match id_opt with
        | None ->
            raise (InvariantBroken (
                Format.asprintf
                  "Michelson contracts must be called with an \
                   entrypoint name. Did you mean %a.default ?"
                  Contract.pp kt1))
        | Some (LDot _ as id) ->
            raise (InvariantBroken (
                Format.asprintf
                  "Invalid entrypoint name for Michelson contract: %a.%a"
                  Contract.pp kt1 Ident.print_strident id))
        | Some (LName name) ->
            let entrypoint =
              Script_ir_translator.parse_toplevel ~legacy:true code
              >>? fun (arg_type, _, _, root_name, _fee_code) ->
              Script_ir_translator.parse_ty ctxt.actxt ~legacy:true
                ~allow_big_map:true ~allow_operation:false
                ~allow_contract:true arg_type
              >>? fun (Ex_ty arg_type, _) ->
              Script_ir_translator.find_entrypoint ~root_name arg_type name
            in
            match entrypoint with
            | Ok (_f, Script_ir_translator.Ex_ty ty) ->
                Script_ir_translator.unparse_ty ctxt.actxt ty
                >>|? fun (ty_node, _) ->
                let actxt, love_ty =
                  match Love_michelson.from_michelson_node_const
                          ctxt.actxt ty_node with
                  | Ok (actxt, Type t) -> actxt, t
                  | Ok (_, Value _) | Error _ ->
                      raise (InvariantBroken (
                          Format.asprintf
                            "Invalid type of Michelson entrypoint: %a.%s"
                            Contract.pp kt1 name))
                in
                { ctxt with actxt },
                Some (CntMichelson (
                    name, Love_type_list.get_type "entrypoint" [love_ty]))
            | Error _ ->
                return (ctxt, None)
      end

let resolve_path_with_kt1 ctxt path (* starting with kt1 *) =
  let split_kt1 path =
    let maybe_kt1, id_opt = Ident.split path in
    match Utils.address_of_string_opt maybe_kt1 with
    | Some kt1 -> Some (kt1, id_opt)
    | None -> None
  in
  match split_kt1 path with
  | Some (kt1, id_opt) -> resolve_path ctxt kt1 id_opt
  | None ->
      raise (InvariantBroken (
          Format.asprintf "Invalid path: should begin with KT1, got %a"
            Ident.print_strident path))

let get_value ctxt path =
  resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
  match res with
  | Some (CntLove (VValue { vvalue_code = v; _ })) -> ctxt, v
  | Some _ | None ->
      raise (InvariantBroken (
          Format.asprintf "Value %a not found" Ident.print_strident path))

let get_struct ctxt path =
  resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
  match res with
  | Some (CntLove (VStructure s)) -> ctxt, s
  | Some _ | None ->
      raise (InvariantBroken (
          Format.asprintf "Structure %a not found" Ident.print_strident path))

let get_sig ctxt path =
  resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
  match res with
  | Some (CntLove (VSignature s)) -> ctxt, s
  | Some _ | None ->
      raise (InvariantBroken (
          Format.asprintf "Signature %a not found" Ident.print_strident path))
