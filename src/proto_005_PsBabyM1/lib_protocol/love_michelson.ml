(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Love_value
open Value
open Error_monad
open Love_type_utils
open Love_ast_types
open Love_ast_types.TYPE

module UC = Michelson_v1_gas.Cost_of.Unparse

let unit ctxt l =
  match l with
    [] -> Gas.consume ctxt UC.unit >>? fun ctxt -> ok (ctxt, VUnit)
  | _ ->
      failwith "Love_michelson: unit value expects no argument"

let bool ctxt b l =
  match l with
    [] -> Gas.consume ctxt UC.bool >>? fun ctxt -> ok (ctxt, VBool b)
  | _ ->
      failwith
        "Love_michelson: true/false values expects no argument"

let constr ctxt s l =
  let tlist =
    List.fold_left
      (fun acc ->
         function
           Love_script_repr.Value v -> acc >>?
             fun l -> ok (v :: l)
         | Type _ ->
             failwith "Constructors do not expect types"
      )
      (ok [])
      l
  in
  tlist >>? fun tlist ->
  Gas.consume ctxt (UC.string s) >>? fun ctxt ->
  ok (ctxt, VConstr (s, List.rev tlist))

let left ctxt l =
  match l with
    [_] -> constr ctxt "Left" l
  | _ ->
      failwith "Love_michelson: Left value expects an argument"

let right ctxt l =
  match l with
    [_] -> constr ctxt "Left" l
  | _ ->
      failwith "Love_michelson: Left value expects an argument"

let some ctxt l =
  match l with
    [_] -> constr ctxt "Some" l
  | _ ->
      failwith "Love_michelson: Some value expects an argument"

let none ctxt l =
  match l with
    [] -> constr ctxt "None" l
  | _ ->
      failwith "Love_michelson: None value does not expect \
                argument"


let tuple ctxt l =
  match l with
    [_;_] ->
      let l =
        List.fold_left
          (fun acc ->
             function
               Love_script_repr.Value v -> acc >>?
                 fun l -> ok (v :: l)
             | Type _ ->
                 failwith "Tuples do not expect types"
          )
          (ok [])
          l
      in l >>? fun l ->
      Gas.consume ctxt UC.pair >>? fun ctxt ->
      ok (ctxt, VTuple l)
  | _ ->
      failwith "Love_michelson: Michelson tuples are pairs"

let value (ctxt, v) = ok (ctxt, Love_script_repr.Value v)

let typ (ctxt, t) = ok (ctxt, Love_script_repr.Type t)

let get_typ ctxt n l =
  let tlist =
    List.fold_left
      (fun acc ->
         function
           Love_script_repr.Type t -> acc >>?
             fun l -> ok (t :: l)
         | Value _ -> failwith "Types do not expect values"
      )
      (ok [])
      l
  in
  tlist >>? fun l ->
  Gas.consume ctxt (UC.string n) >>? fun ctxt ->
  try ok (ctxt, Love_type_list.get_type n (List.rev l))
  with Love_pervasives.Exceptions.BadArgument s -> failwith s

let lambda ctxt args =
  match args with
    [Love_script_repr.Type t1; Type t2] ->
      Gas.consume ctxt UC.pair >>? fun ctxt ->
      ok (ctxt, t1 @=> t2)
  | _ -> failwith "Lambda expects 2 types"

let pair ctxt args =
  match args with
    [Love_script_repr.Type t1; Type t2] ->
      Gas.consume ctxt UC.pair >>? fun ctxt ->
      ok (ctxt, TTuple [t1;t2])
  | _ -> failwith "Michelson tuples expects 2 types"

let from_michelson_prim ctxt (args : Love_script_repr.const list) =
  function
  | Michelson_v1_primitives.D_False ->
      bool ctxt false args         >>? value
  | D_Left -> left ctxt args       >>? value
  | D_None -> none ctxt args       >>? value
  | D_Pair -> tuple ctxt args      >>? value
  | D_Right -> right ctxt args     >>? value
  | D_Some -> some ctxt args       >>? value
  | D_True -> bool ctxt false args >>? value
  | D_Unit -> unit ctxt args       >>? value

  | T_bool ->       get_typ ctxt "bool" args      >>? typ
  | T_int ->        get_typ ctxt "int" args       >>? typ
  | T_key ->        get_typ ctxt "key" args       >>? typ
  | T_key_hash ->   get_typ ctxt "keyhash" args   >>? typ
  | T_lambda ->     lambda ctxt args              >>? typ
  | T_list ->       get_typ ctxt "list" args      >>? typ
  | T_map ->        get_typ ctxt "map" args       >>? typ
  | T_big_map ->    get_typ ctxt "bigmap" args    >>? typ
  | T_nat ->        get_typ ctxt "nat" args       >>? typ
  | T_option ->     get_typ ctxt "option" args    >>? typ
  | T_or ->         get_typ ctxt "variant" args   >>? typ
  | T_pair ->       pair ctxt args                >>? typ
  | T_set ->        get_typ ctxt "set" args       >>? typ
  | T_signature ->  get_typ ctxt "signature" args >>? typ
  | T_string ->     get_typ ctxt "string" args    >>? typ
  | T_bytes ->      get_typ ctxt "bytes" args     >>? typ
  | T_mutez ->      get_typ ctxt "dun" args       >>? typ
  | T_timestamp  -> get_typ ctxt "timestamp" args >>? typ
  | T_unit ->       get_typ ctxt "unit" args      >>? typ
  | T_operation ->  get_typ ctxt "operation" args >>? typ
  | T_address ->    get_typ ctxt "address" args   >>? typ

  | K_parameter
  | K_storage
  | K_code
  | D_Elt
  | I_PACK
  | I_UNPACK
  | I_BLAKE2B
  | I_SHA256
  | I_SHA512
  | I_ABS
  | I_ADD
  | I_AMOUNT
  | I_AND
  | I_BALANCE
  | I_CAR
  | I_CDR
  | I_CHAIN_ID
  | I_CHECK_SIGNATURE
  | I_COMPARE
  | I_CONCAT
  | I_CONS
  | I_CREATE_ACCOUNT
  | I_CREATE_CONTRACT
  | I_IMPLICIT_ACCOUNT
  | I_DIP
  | I_DROP
  | I_DUP
  | I_EDIV
  | I_EMPTY_BIG_MAP
  | I_EMPTY_MAP
  | I_EMPTY_SET
  | I_EQ
  | I_EXEC
  | I_APPLY
  | I_FAILWITH
  | I_GE
  | I_GET
  | I_GT
  | I_HASH_KEY
  | I_IF
  | I_IF_CONS
  | I_IF_LEFT
  | I_IF_NONE
  | I_INT
  | I_LAMBDA
  | I_LE
  | I_LEFT
  | I_LOOP
  | I_LSL
  | I_LSR
  | I_LT
  | I_MAP
  | I_MEM
  | I_MUL
  | I_NEG
  | I_NEQ
  | I_NIL
  | I_NONE
  | I_NOT
  | I_NOW
  | I_OR
  | I_PAIR
  | I_PUSH
  | I_RIGHT
  | I_SIZE
  | I_SOME
  | I_SOURCE
  | I_SENDER
  | I_SELF
  | I_SLICE
  | I_STEPS_TO_QUOTA
  | I_SUB
  | I_SWAP
  | I_TRANSFER_TOKENS
  | I_SET_DELEGATE
  | I_UNIT
  | I_UPDATE
  | I_XOR
  | I_ITER
  | I_LOOP_LEFT
  | I_ADDRESS
  | I_CONTRACT
  | I_DUNE_PRIM _
  | I_ISNAT
  | I_CAST
  | I_RENAME
  | I_DIG
  | I_DUG
  | T_contract
  | T_chain_id as prim ->
      failwith
        (Format.asprintf
           "Love_michelson: \
            primitive %s has no equivalent in Love"
           (Michelson_v1_primitives.string_of_prim prim)
        )

let rec from_michelson_node_const ctxt m =
  let open Micheline in
  match m with
  | Int (_, n) -> ok (ctxt, VInt n) >>? value
  | String (_, s) -> ok (ctxt, VString s) >>? value
  | Bytes (_, b) -> ok (ctxt, VBytes b) >>? value
  | Prim (_, prim, args, _) ->
      begin
        let rec loop ctxt acc = function
            [] -> ok (ctxt, List.rev acc)
          | hd :: tl ->
              from_michelson_node_const ctxt hd >>?
              fun (ctxt, hd') -> loop ctxt (hd' :: acc) tl
        in
        loop ctxt [] args >>? fun (ctxt, args) ->
         from_michelson_prim ctxt args prim
      end
  | Seq (_, _) ->
      failwith "Love_michelson: No sequences allowed"

let from_michelson_const ctxt m =
  from_michelson_node_const ctxt @@ Micheline.root m

let pair e1 e2 =
  Micheline.Prim (
    0,
    Michelson_v1_primitives.D_Pair,
    [e1; e2],
    []
  )

let constr n l =
  let prim = match n with
      "Left" -> ok Michelson_v1_primitives.D_Left
    | "Right" -> ok Michelson_v1_primitives.D_Right
    | "Some" -> ok Michelson_v1_primitives.D_Some
    | "None" -> ok Michelson_v1_primitives.D_None
    | _ -> failwith ("Love_michelson: Unknown constructor " ^ n)
  in
  prim >>? (fun p ->
      ok (Micheline.Prim (0, p, l, [])))

let rec to_michelson_value v =
  match v with
    VInt i | VNat i -> ok (Micheline.Int (0, i))
  | VString s -> ok (Micheline.String (0, s))
  | VBytes b -> ok (Micheline.Bytes (0, b))
  | VTuple l ->
      begin
        let rec loop = function
            [] | [_] ->
              failwith
                "Love_michelson: Tuples expect at least 2 elements"
          | [e1; e2] ->
              to_michelson_value e1 >>? (fun e1 ->
                  to_michelson_value e2 >>? (fun e2 ->
                      ok (pair e1 e2)))
          | hd :: tl ->
              loop tl >>? (fun new_tl ->
                  to_michelson_value hd >>? (fun new_hd ->
                      ok (pair new_hd new_tl)))
        in
        loop l
      end
  | VConstr (n, l) ->
      begin
        let rec loop acc = function
            [] -> ok (List.rev acc)
          | hd :: tl ->
              to_michelson_value hd >>?
              fun hd -> loop (hd :: acc) tl
        in loop [] l >>? fun args -> constr n args
      end
  | VUnit -> ok (
      Micheline.Prim (
        0,
        Michelson_v1_primitives.D_Unit,
        [],
        []
      )
    )
  | VBool b -> ok (
      Micheline.Prim (
        0,
        (if b then
           Michelson_v1_primitives.D_True
         else D_False),
        [],
        []
      )
    )
  | _ ->
      failwith
        "Love_michelson: Value not convertible in Michelson"

let prim p args = Micheline.Prim (0, p, args, [])

let rec to_michelson_type t =
  let open Michelson_v1_primitives in
  match t with
  | TUser (LName "bool", []) -> ok @@ prim T_bool []
  | TUser (LName "int", [])  -> ok @@ prim T_int  []
  | TUser (LName "key", [])  -> ok @@ prim T_key  []
  | TUser (LName "keyhash", []) -> ok @@ prim T_key_hash []
  | TUser (LName "list", [t]) ->
      to_michelson_type t >>?
      fun arg ->
      ok @@ prim T_list [arg]

  | TUser (LName "set", [t1]) ->
      to_michelson_type t1 >>? fun t1 ->
      ok @@ prim T_set [t1]

  | TUser (LName "map", [t1; t2]) ->
      to_michelson_type t1 >>? fun t1 ->
      to_michelson_type t2 >>? fun t2 ->
      ok @@ prim T_map [t1; t2]

  | TUser (LName "bigmap", [t1; t2]) ->
      to_michelson_type t1 >>? fun t1 ->
      to_michelson_type t2 >>? fun t2 ->
      ok @@ prim T_big_map [t1; t2]

  | TUser (LName "nat", []) -> ok @@ prim T_nat  []
  | TUser (LName "option", [t]) ->
      to_michelson_type t >>? fun t -> ok @@ prim T_option [t]

  | TUser (LName "variant", [t1; t2]) ->
      to_michelson_type t1 >>? fun t1 ->
      to_michelson_type t2 >>? fun t2 ->
      ok @@ prim T_or [t1; t2]

  | TTuple l ->
      begin
        let rec loop =
          function
            [] | [_] ->
              failwith "Love_michelson: \
                        Love tuples expect at least 2 types"
          | [t1; t2] ->
              to_michelson_type t1 >>? fun t1 ->
              to_michelson_type t2 >>? fun t2 ->
              ok @@ prim T_pair [t1; t2]
          | hd :: tl ->
              loop tl >>? fun res ->
              to_michelson_type hd >>? fun hd ->
              ok @@ prim T_pair [hd; res]
        in loop l
      end
  | TUser (LName "signature", []) -> ok @@ prim T_signature  []
  | TUser (LName "string", []) -> ok @@ prim T_string  []
  | TUser (LName "bytes", []) -> ok @@ prim T_bytes []
  | TUser (LName "dun", [])  -> ok @@ prim T_mutez  []
  | TUser (LName "timestamp", [])  -> ok @@ prim T_timestamp  []
  | TUser (LName "unit", [])  -> ok @@ prim T_unit  []
  | TUser (LName "operation", [])  -> ok @@ prim T_operation  []
  | TUser (LName "address", [])  -> ok @@ prim T_address  []

  | TArrow (t1, t2) ->
      begin
        to_michelson_type t1 >>?
        fun t1 -> to_michelson_type t2 >>?
        fun t2 -> ok @@ prim T_lambda [t1; t2]
      end

  | _ -> failwith "Love_michelson: Unknown type"

let to_michelson_const = function
    Love_script_repr.Value v -> to_michelson_value v
  | Type t -> to_michelson_type t

let to_michelson_const (v : Love_script_repr.const) : Script_repr.expr tzresult =
  to_michelson_const v >>? fun v ->
  ok @@ Micheline.strip_locations v

let michelson_contract_as_sig actxt (expr : Script_repr.expr) =
  begin
    Script_ir_translator.parse_toplevel ~legacy:true expr >>?
    fun (arg_type, _, _, root_name, _fee_code) ->
    Script_ir_translator.parse_ty actxt ~legacy:true
      ~allow_big_map:true ~allow_operation:false
      ~allow_contract:true arg_type >>?
    fun (Script_ir_translator.Ex_ty arg_type, _) ->
    Script_ir_translator.list_entrypoints ~root_name arg_type actxt
  end >>? fun (_,map) ->
  Script_ir_translator.Entrypoints_map.fold
    (fun entry (_,ty) acc ->
        acc >>? fun (actxt, acc) ->
        from_michelson_node_const actxt ty >>?
        function
          ctxt, Type ty -> ok @@ (ctxt, ((entry, TYPE.SEntry ty) :: acc))
        | _, Value _ -> raise (
            Love_pervasives.Exceptions.InvariantBroken
              "Entrypoint type should not be a value"
          )
    )
    map
    (ok (actxt, []))
  >>? fun (actxt, sig_content) ->
  ok (actxt, TYPE.{sig_kind = Contract []; sig_content})

(** Revision test for exported functions *)

let with_rev ctxt cont =
  if Dune_storage.has_protocol_revision ctxt 3
  then cont ()
  else failwith "Love_michelson: Bad revision, expected at least 3"

let to_michelson_type ctxt t = with_rev ctxt (fun _ -> to_michelson_type t)

let to_michelson_const ctxt c = with_rev ctxt (fun _ -> to_michelson_const c)

let from_michelson_node_const ctxt n = with_rev ctxt (fun _ -> from_michelson_node_const ctxt n)

let michelson_contract_as_sig ctxt n = with_rev ctxt (fun _ -> michelson_contract_as_sig ctxt n)

let from_michelson_const ctxt n = with_rev ctxt (fun _ -> from_michelson_const ctxt n)
