import os
import subprocess
import pytest
from tools import utils, paths, constants

CONTRACT_PATH = f'{paths.TEZOS_HOME}/src/bin_client/test/contracts'

BAKE_ARGS = ['--minimal-timestamp']


def file_basename(path):
    return os.path.splitext(os.path.basename(path))[0]


def originate(client, contract, init_storage, amount, contract_name=None,
              sender='bootstrap1', baker='bootstrap5') -> str:
    if contract_name is None:
        contract_name = file_basename(contract)
    args = ['--init', init_storage, '--burn-cap', '10.0']
    origination = client.originate(contract_name, amount, sender, contract,args)
    print(origination.contract)
    client.bake(baker, BAKE_ARGS)
    assert utils.check_block_contains_operations(
        client, [origination.operation_hash])
    return origination.contract


def transfer(client, amount, from_, to, entry, arg, baker='bootstrap5'):
    args = [ '--entrypoint', entry, '--arg', arg, '--burn-cap', '10.0']
    transfer = client.transfer(amount, from_, to, args)
    client.bake(baker, BAKE_ARGS)
    assert utils.check_block_contains_operations(
        client, [transfer.operation_hash])


def get_storage(client, contract) -> dict:
    url = f'/chains/main/blocks/head/context/contracts/{contract}/storage'
    res = client.rpc('get', url)
    return res['dune_expr']


def get_field(r : dict, f : str) -> dict:
    return r['record'][f]

def get_some(x : dict) -> dict:
    assert(x['constr'][0] == 'Some')
    return x['constr'][1][0]


def open_addr(x : dict) -> str:
    return x['addr']


def none() -> dict:
    return { 'constr': ['None', None] }

def some(x : dict) -> dict:
    return { 'constr': ['Some', [x]] }

def int(x : int) -> dict:
    return { 'int': str(x) }

def addr(x : str) -> dict:
    return { 'addr': x }


@pytest.mark.contract
class TestInteraction:

    def test_interaction_origination(self, client, session):
        path = f'{CONTRACT_PATH}/love/interaction.lov'
        init = '#love:{ cnt = None; value = -1 }'
        session['contract'] = originate(client, path, init, 0)
        r = get_storage(client, session['contract'])
        assert(get_field(r, 'cnt') == none())
        assert(get_field(r, 'value') == int(-1))

    def test_interaction_call_deploy(self, client, session):
        transfer(client, 0, 'bootstrap2', 'interaction', 'deploy', '#love:42')
        r = get_storage(client, session['contract'])
        assert(get_field(r, 'value') == int(-1))
        assert(get_field(r, 'cnt')['constr'][0] == 'Some')
        session['child_contract'] = open_addr(get_some(get_field(r, 'cnt')))
        r = get_storage(client, session['child_contract'])
        assert(r == int(42))

    def test_interaction_call_set(self, client, session):
        transfer(client, 0, 'bootstrap2', 'interaction', 'call_set', '#love:14')
        r = get_storage(client, session['contract'])
        assert(get_field(r, 'value') == int(-1))
        assert(get_field(r, 'cnt') == some(addr(session['child_contract'])))
        r = get_storage(client, session['child_contract'])
        assert(r == int(14))

    def test_interaction_call_get(self, client, session):
        transfer(client, 0, 'bootstrap2', 'interaction', 'call_get', '#love:()')
        r = get_storage(client, session['contract'])
        r = get_storage(client, session['contract'])
        assert(get_field(r, 'value') == int(14))
        assert(get_field(r, 'cnt') == some(addr(session['child_contract'])))
        r = get_storage(client, session['child_contract'])
        assert(r == int(14))
