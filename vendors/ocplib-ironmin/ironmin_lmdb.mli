(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

include Ironmin.IronminStorage

type config = {
  mapsize : int64 option ;
  readonly : bool option ;
  root : string ;
}

val create_config : ?mapsize:int64 -> ?readonly:bool -> string -> config
val create_storage : config -> t


module KeyValueStore : sig
  include Ironmin.IronminKeyValue

  val create : ?mapsize:int64 -> ?readonly:bool -> string -> t
end
