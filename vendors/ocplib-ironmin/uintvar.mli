(**************************************************************************)
(*                                                                        *)
(*  Copyright Origin Labs 2019                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* Encoding of integers with variable length, 7-bit per byte encoding.
   Zero to 127 are encoded on '\000'-'\127'.
*)

val len_uint64 : int64 -> int
val len_uint : int -> int

module STRING : sig
  val get_uint8 : string -> int -> int * int
  val get_uint64 : string -> int -> int64 * int
  val get_uint : string -> int -> int * int
end

module BIGSTRING : sig
  val get_uint8 : Bigstring.t -> int -> int * int
  val get_uint64 : Bigstring.t -> int -> int64 * int
  val get_uint : Bigstring.t -> int -> int * int
end

module BYTES : sig
  val set_uint8 : bytes -> int -> int -> int
  val set_uint64 : bytes -> int -> int64 -> int
  val set_uint : bytes -> int -> int -> int

  val get_uint8 : bytes -> int -> int * int
  val get_uint64 : bytes -> int -> int64 * int
  val get_uint : bytes -> int -> int * int
end

module MAKE_CONSUMER_GET(S: sig
    type t
    val get_uint8 : t -> int
  end): sig
  val get_uint64 : S.t -> int64
  val get_uint : S.t -> int
end

module MAKE_STREAM_GET(S: sig
    type t
    type b
    type 'a s
    val get_uint8 : (b -> 'a s) -> t -> (int * t -> 'a s) -> 'a s
  end): sig
  val get_uint64 : (S.b -> 'a S.s) -> S.t -> (int64 * S.t -> 'a S.s) -> 'a S.s
  val get_uint : (S.b -> 'a S.s) -> S.t -> (int * S.t -> 'a S.s) -> 'a S.s
end

module MAKE_SET(S: sig
    type t
    val set_uint8 : t -> int -> int -> unit
  end): sig
  val set_uint64 : S.t -> int -> int64 -> int
  val set_uint : S.t -> int -> int -> int
end

module MAKE_GET(S: sig
    type t
    val get_uint8 : t -> int -> int
  end): sig
  val get_uint64 : S.t -> int -> int64
  val get_uint : S.t -> int -> int
end
